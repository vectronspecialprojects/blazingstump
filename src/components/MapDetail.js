import React from 'react'
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {responsiveHeight, responsiveWidth} from '../Themes/Metrics'
import Styles from '../Themes/Styles'
import Colors from '../Themes/Colors'
import MapPreview from './MapPreview'
import {navigate} from '../navigation/NavigationService';
import RouteKey from '../navigation/RouteKey';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import {getIconsName} from '../utilities/utils';

const MapDetail = (props) => {
  return (
    <View style={{width: '100%', minHeight: 400}}>
      <MapPreview onPress={props.onMapPress} style={styles.MapPreview} location={props.location}>
        <Text style={Styles.smallNormalText}>location not found</Text>
      </MapPreview>
      <View style={{width: '100%', padding: responsiveWidth(5), flexDirection: 'row'}}>
        <View style={{flex: 2, height: '100%', paddingLeft: responsiveWidth(10)}}>
          {props.address && (
            <Text
              style={{
                ...Styles.xSmallNormalText,
                ...{
                  lineHeight: responsiveHeight(20),
                  marginBottom: responsiveHeight(10),
                  textAlign: 'left',
                  color: Colors.location.description,
                },
              }}>
              {props.address}
            </Text>
          )}
          {props.telp && (
            <Text
              style={{...Styles.xSmallNormalText, ...{textAlign: 'left', color: Colors.location.description}}}
              onPress={props.onTelpPress}>
              T: {props.telp}
            </Text>
          )}
          {props.email && (
            <Text
              style={{...Styles.xSmallNormalText, ...{textAlign: 'left', color: Colors.location.description}}}
              onPress={props.onEmailPress}>
              E: {props.email}
            </Text>
          )}
        </View>
        <View style={{flex: 1}}>
          <Text onPress={props.onDirectionsPress} style={{...Styles.smallUpBoldText, ...styles.directions}}>
            directions{'>'}
          </Text>
        </View>
      </View>
      <View
        style={{
          width: '100%',
          padding: responsiveWidth(20),
          flexDirection: 'row',
          justifyContent: 'space-between',
        }}>
        {!props?.isHideOpenHours && (
          <View style={{maxWidth: '50%', padding: responsiveWidth(5), marginBottom: responsiveHeight(10)}}>
            <Text
              style={{
                ...Styles.smallUpBoldText,
                ...{alignSelf: 'center', color: Colors.location.openHours, marginBottom: responsiveHeight(4)},
              }}>
              openning hours
            </Text>
            <View style={{width: '100%'}}>
              {props.openHours?.map((h) => {
                return (
                  <Text
                    key={h.day}
                    style={{
                      ...Styles.xSmallNormalText,
                      ...{alignSelf: 'center', color: Colors.location.description},
                    }}>
                    {h.day} {h.time}
                  </Text>
                )
              })}
            </View>
          </View>
        )}
        {!props?.isHideDeliveryHours && (
          <View style={{maxWidth: '50%', padding: responsiveWidth(5), marginBottom: responsiveHeight(10)}}>
            <Text
              style={{
                ...Styles.smallUpBoldText,
                ...{alignSelf: 'center', color: Colors.location.openHours, marginBottom: responsiveHeight(4)},
              }}>
              delivery hours
            </Text>
            <View style={{width: '100%'}}>
              {props.DeliveryHours?.map((h) => {
                return (
                  <Text
                    key={h.day}
                    style={{
                      ...Styles.xSmallNormalText,
                      ...{alignSelf: 'center', color: Colors.location.description},
                    }}>
                    {h.day} {h.time}
                  </Text>
                )
              })}
            </View>
          </View>
        )}
      </View>
      <View style={styles.socialContainer}>
        {[...(props.socialLinks || []), ...(props.menuLinks || [])]?.map((item, index) => {
          if (!item.url) return null
          return (
            <TouchableOpacity
              style={styles.socialItem}
              key={index.toString()}
              onPress={() => {
                navigate(RouteKey.WebviewScreen, {
                  params: {
                    appUri: item.url,
                  },
                })
              }}>
              <FontAwesome5 name={getIconsName(item.platform)} size={20} color={Colors.second} />
            </TouchableOpacity>
          )
        })}
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  MapPreview: {
    width: '100%',
    height: 200,
  },
  directions: {
    position: 'absolute',
    bottom: 0,
    marginVertical: 10,
    paddingHorizontal: 8,
    color: Colors.location.description,
  },
  socialItem: {
    width: responsiveWidth(30),
    height: responsiveWidth(30),
    borderRadius: responsiveWidth(20),
    borderWidth: 1,
    borderColor: Colors.second,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: responsiveWidth(10),
  },
  socialContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
})

export default MapDetail
