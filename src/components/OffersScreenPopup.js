import React from 'react'
import {View, Text, StyleSheet, Image} from 'react-native'
import {TouchableCmp} from './UtilityFunctions'
import {responsiveHeight, responsiveWidth, deviceWidth} from '../Themes/Metrics'
import Modal from 'react-native-modal'
import Styles from '../Themes/Styles'
import Colors from '../Themes/Colors'

const OffersScreenPopup = (props) => {
  if (props.renderItem) {
    return (
      <Modal isVisible={props.isVisible} animationIn="swing" animationOut="zoomOut">
        <View style={styles.popupContainer}>
          <View style={{width: '100%', aspectRatio: 1}}>
            <Image
              style={{
                flex: 1,
                width: '100%',
                resizeMode: 'cover',
              }}
              source={{uri: props.renderItem.image_square}}
            />
          </View>

          <View style={{padding: responsiveWidth(20)}}>
            <Text style={[Styles.mediumCapBoldText, {color: Colors.offer.title}]}>
              {props.renderItem.heading}
            </Text>
            {(props?.renderItem?.products?.length !== 0) && <Text
              style={{
                ...Styles.mediumCapBoldText,
                ...{
                  marginBottom: responsiveHeight(20),
                  color: Colors.offer.point,
                  marginTop: responsiveHeight(5),
                },
              }}>
              {+props.renderItem?.products?.[0]?.product?.point_price} Points
            </Text>}
            <Text
              style={{...Styles.xSmallCapText, ...{textAlign: 'center', color: Colors.offer.description}}}>
              {props.renderItem.desc_short}
            </Text>
          </View>

          <View style={styles.popupButtonsBar}>
            <View style={styles.button}>
              <TouchableCmp style={styles.buttonTouch} onPress={props.onCancelPress}>
                <Text style={Styles.mediumCapBoldText} numberOfLines={1}>
                  Cancel
                </Text>
              </TouchableCmp>
            </View>
            <View style={[styles.button, (props?.renderItem?.products?.length===0) && {opacity: 0.5}]}>
              <TouchableCmp style={styles.buttonTouch} onPress={props.onOkPress} disabled={(props?.renderItem?.products?.length!==0)?false:true} >
                <Text
                  style={{...Styles.mediumCapBoldText, ...{color: Colors.offer.btnRedeem}}}
                  numberOfLines={1}>
                  Redeem
                </Text>
              </TouchableCmp>
            </View>
          </View>
        </View>
      </Modal>
    )
  }
  return null
}

const styles = StyleSheet.create({
  popupContainer: {
    alignSelf: 'center',
    width: deviceWidth() * 0.7,
    backgroundColor: Colors.first,
    borderRadius: 15,
    overflow: 'hidden',
  },
  popupButtonsBar: {
    width: '100%',
    flexDirection: 'row',
    height: responsiveHeight(50),
    justifyContent: 'space-between',
  },
  button: {
    flex: 1,
    maxWidth: '49.5%',
    backgroundColor: Colors.offer.buttonBackground,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonTouch: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
})

export default OffersScreenPopup
