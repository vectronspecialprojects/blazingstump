import React from "react";
import { View, ActivityIndicator, StyleSheet } from "react-native";

import SubHeaderBar from "./SubHeaderBar";
import Styles from "../Themes/Styles";
import Colors from "../Themes/Colors";

const LoadingPage = (props) => {
  return (
    <View style={Styles.screen}>
      <SubHeaderBar title = {props.title}/>
      <View style={styles.loadingContainer}>
        <ActivityIndicator size="large" color={Colors.defaultRefreshSpinner} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  loadingContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
});

export default LoadingPage;
