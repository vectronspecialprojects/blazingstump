import React, {useMemo} from 'react'
import {View, ImageBackground, StyleSheet, Text} from 'react-native'
import {TouchableCmp} from './UtilityFunctions'
import {responsiveHeight, responsiveFont, responsiveWidth} from '../Themes/Metrics'
import Colors from '../Themes/Colors'
import Styles from '../Themes/Styles'
import Ionicons from 'react-native-vector-icons/Ionicons'
import CardHeader from './CardHeader'
import DateBar from './DateBar'

const CardItem = (props) => {
  const eventDays = JSON.parse(props.cardDetail.eventDays).occurrence
  const emptyStars = props.cardDetail.emptyStars
  const filledStars = props.cardDetail.filledStarts
  const showDetail = props.showDetail === undefined ? true : props.showDetail

  const emptyStarsShow = useMemo(() => {
    let data = []
    for (let i = 1; i <= emptyStars; i++) {
      data.push(
        <Ionicons
          key={i}
          name="ios-star-outline"
          size={responsiveFont(26)}
          style={{color: Colors.stampcard.starIcon, paddingHorizontal: responsiveWidth(5)}}
        />,
      )
    }
    return data
  }, [emptyStars])

  const filledStarsShow = useMemo(() => {
    let data = []
    for (let i = 1; i <= filledStars; i++) {
      data.push(
        <Ionicons
          key={i}
          name="ios-star"
          size={responsiveFont(26)}
          style={{color: Colors.stampcard.starIcon, paddingHorizontal: responsiveWidth(5)}}
        />,
      )
    }
    return data
  }, [filledStars])

  return (
    <ImageBackground style={{...styles.imageBanner, ...props.style}} source={{uri: props.cardDetail.image}}>
      <TouchableCmp
        disabled={props.touchDisabled}
        activeOpacity={0.6}
        style={{flex: 1}}
        onPress={props.onPress}>
        <View style={[styles.cardContainer, !showDetail && {backgroundColor: 'rgba(0, 0, 0, 0)'}]}>
          {showDetail && (
            <CardHeader
              title={props.cardDetail.title}
              titleStyle={props.titleStyle}
              style={{marginBottom: responsiveHeight(5)}}
            />
          )}
          {showDetail && (
            <Text style={[Styles.xSmallUpText, {color: Colors.whaton.description}]}>
              {props.cardDetail.description}
            </Text>
          )}
          {eventDays && showDetail && <DateBar days={eventDays} style={{marginTop: responsiveHeight(5)}} color={Colors.whaton.dateColor} />}
          {(filledStarsShow || emptyStarsShow) && showDetail && (
            <View style={styles.starContainer}>
              {filledStarsShow}
              {emptyStarsShow}
            </View>
          )}
        </View>
      </TouchableCmp>
    </ImageBackground>
  )
}

const styles = StyleSheet.create({
  imageBanner: {
    width: '100%',
    height: responsiveHeight(120),
  },
  cardContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.6)',
    paddingHorizontal: responsiveWidth(5)
  },
  starContainer: {
    flexDirection: 'row',
  },
})

export default CardItem
