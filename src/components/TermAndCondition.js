import React, {useState} from 'react'
import {View, StyleSheet, Text} from 'react-native'
import Styles from '../Themes/Styles'
import Colors from '../Themes/Colors'
import {useSelector} from 'react-redux'
import HtmlTextPopup from './HtmlTextPopup'
import {responsiveWidth} from '../Themes/Metrics';
import {venueName} from '../constants/env'

function TermAndCondition() {
  const [showData, setShowData] = useState({header: '', html: ''})
  const legals = useSelector((state) => state.infoServices.legals)
  const [isPopupVisible, setIsPopupVisible] = useState(false)
  const legalsPopup = (cases) => {
    setIsPopupVisible(true)
    if (cases === 'termAndCondition') {
      setShowData({header: legals[1][0], html: legals[1][1]})
    } else if (cases === 'policy') {
      setShowData({header: legals[0][0], html: legals[0][1]})
    }
  }

  return (
    <View>
      <View style={styles.termContainer}>
        <Text style={Styles.xSmallNormalText}>
          <Text>By creating an account, you agree To {venueName}'s </Text>
          <Text
            onPress={() => {
              legalsPopup('termAndCondition')
            }}
            style={{color: Colors.termAndConditionTextColor, textDecorationLine: 'underline'}}>
            Term & Conditions
          </Text>
          <Text> and </Text>
          <Text
            onPress={() => {
              legalsPopup('policy')
            }}
            style={{color: Colors.termAndConditionTextColor, textDecorationLine: 'underline'}}>
            Privacy Policy.
          </Text>
        </Text>
      </View>
      <HtmlTextPopup
        isVisible={isPopupVisible}
        header={showData.header}
        html={showData.html}
        onOkPress={() => setIsPopupVisible(false)}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  termContainer: {
    alignItems: 'center',
    paddingHorizontal: responsiveWidth(20),
  },
})

export default TermAndCondition
