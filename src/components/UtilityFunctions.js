import React from 'react'
import {TouchableOpacity, TouchableNativeFeedback, Platform, Linking} from 'react-native'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import Entypo from 'react-native-vector-icons/Entypo'

export const TouchableCmp = (props) => {
  let TouchableCmp = TouchableOpacity
  if (Platform.OS === 'android' && Platform.Version >= 21) {
    TouchableCmp = TouchableNativeFeedback
  }
  return <TouchableCmp {...props} />
}

export const FontAwesomeTouch = (props) => {
  return (
    <TouchableCmp
      onPress={props.onPress}
      style={props.style}
      activeOpacity={props.activeOpacity}
      disabled={props.disabled}>
      <FontAwesome name={props.name} size={props.size} color={props.color} />
    </TouchableCmp>
  )
}

export const FontAwesome5Touch = (props) => {
  return (
    <TouchableCmp
      onPress={props.onPress}
      style={props.style}
      activeOpacity={props.activeOpacity}
      disabled={props.disabled}>
      <FontAwesome5 name={props.name} size={props.size} color={props.color} />
    </TouchableCmp>
  )
}

export const EntypoTouch = (props) => {
  return (
    <TouchableCmp
      onPress={props.onPress}
      style={props.style}
      activeOpacity={props.activeOpacity}
      disabled={props.disabled}>
      <Entypo name={props.name} size={props.size} color={props.color} />
    </TouchableCmp>
  )
}

export const wait = (timeout) => {
  return new Promise((resolve) => {
    setTimeout(resolve, timeout)
  })
}

export const onLinkPress = (request, href) => {
  console.log(href)
  if (href === 'about:blank') return true
  Linking.openURL(href)
  return false
}
