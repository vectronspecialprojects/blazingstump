import React, {useMemo, useState} from 'react'
import {Text, StyleSheet, View, TouchableOpacity, FlatList, Platform, Modal} from 'react-native'
import TextInputView from './TextInputView'
import {
  deviceHeight,
  deviceWidth,
  hitSlop,
  isIOS,
  responsiveFont,
  responsiveHeight,
  responsiveWidth,
} from '../Themes/Metrics'
import Colors from '../Themes/Colors'
import Fonts from '../Themes/Fonts'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

export default function DropDownList({
  visible,
  onClose,
  data,
  hasSearchBar,
  onSelect,
  value,
  modalTitle,
  style,
  textStyle,
  height = deviceHeight(),
  showValue,
  listEmptyText = '',
  children,
}) {
  const [keyWord, setKeyword] = useState('')

  const filterList = useMemo(() => {
    if (!keyWord) {
      return data
    } else {
      let listGroup = []
      const formatKeyword = keyWord.trim().toLowerCase()
      listGroup = data.filter((item) => {
        const selected = formatShowValue(item)
        return selected.toLowerCase().includes(formatKeyword)
      })
      return listGroup
    }
  }, [keyWord, data])

  function formatShowValue(item) {
    if (typeof showValue === 'string') return item[showValue]
    else if (Array.isArray(showValue)) return showValue.map((value) => item[value]).join(', ')
    else return item
  }

  const renderItem = ({item, index}) => {
    const selected = formatShowValue(item)

    return (
      <TouchableOpacity
        key={item.id}
        style={styles.itemContainer}
        onPress={() => {
          onSelect(item)
          onClose()
          setKeyword('')
        }}>
        <Text
          style={{
            fontFamily: value === selected ? Fonts.openSansBold : Fonts.openSans,
            fontSize: responsiveFont(15),
            marginVertical: 2,
            flex: 1,
          }}>
          {selected}
        </Text>
        {value === selected && <MaterialIcons name={'check'} size={22} color={'#62CAA4'} />}
      </TouchableOpacity>
    )
  }
  return (
    <Modal visible={visible} animationType={'slide'} transparent={true} onRequestClose={onClose}>
      <TouchableOpacity
        onPress={() => {
          onClose()
          setKeyword('')
        }}
        style={styles.container}
        activeOpacity={1}>
        <View style={{...styles.wrapper, height}}>
          {modalTitle && (
            <View style={styles.titleWrapper}>
              <TouchableOpacity
                hitSlop={hitSlop}
                onPress={() => {
                  setKeyword('')
                  onClose()
                }}
                style={{position: 'absolute', zIndex: 999}}>
                <MaterialIcons name={'close'} size={30} color={Colors.gray} />
              </TouchableOpacity>
              <Text style={styles.textSemi}>{modalTitle}</Text>
            </View>
          )}
          {hasSearchBar && (
            <TextInputView
              placeholder={'Search'}
              inputStyle={{borderColor: Colors.white, borderWidth: 1, borderRadius: 4}}
              value={keyWord}
              onChangeText={(text) => setKeyword(text)}
              rightIcon={<MaterialIcons name={'search'} size={20} color={Colors.white} />}
            />
          )}
          {!!children ? (
            children
          ) : (
            <FlatList
              showsVerticalScrollIndicator={false}
              bounces={false}
              keyExtractor={(item, index) => index.toString()}
              keyboardShouldPersistTaps={'handled'}
              data={filterList}
              renderItem={renderItem}
              ItemSeparatorComponent={() => <View style={{height: 1, backgroundColor: Colors.defaultBackground}} />}
              ListEmptyComponent={
                <Text style={{fontSize: responsiveFont(14), marginTop: responsiveHeight(20)}}>
                  {listEmptyText}
                </Text>
              }
            />
          )}
        </View>
      </TouchableOpacity>
    </Modal>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  wrapper: {
    backgroundColor: Colors.white,
    paddingBottom: responsiveHeight(25),
    borderTopLeftRadius: responsiveWidth(20),
    borderTopRightRadius: responsiveWidth(20),
    paddingHorizontal: responsiveWidth(15),
    borderRadius: 5,
  },
  titleWrapper: {
    backgroundColor: Colors.white,
    paddingVertical: responsiveHeight(10),
    alignItems: 'center',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    marginTop: responsiveHeight(10),
    flexDirection: 'row',
  },
  text: {
    flex: 1,
    color: Colors.black,
    paddingBottom: isIOS() ? 0 : 5,
    textAlignVertical: 'center',
    fontSize: responsiveFont(14),
    paddingLeft: responsiveWidth(10),
  },
  headerTitle: {
    color: Colors.primary,
    fontSize: responsiveFont(14),
  },
  textSemi: {
    fontSize: responsiveFont(18),
    textAlign: 'center',
    flex: 1,
  },
  itemContainer: {
    minHeight: 40,
    backgroundColor: Colors.white,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: responsiveWidth(10),
  },
})
