import React from 'react'
import {View, Text, StyleSheet} from 'react-native'
import HTML from 'react-native-render-html'
import {responsiveFont} from '../Themes/Metrics'


const Html = (props) => (
  <HTML
    source={{
      html: props.html
        .replace(/&lt;/g, '<')
        .replace(/&gt;/g, '>')
        .replace(/&nbsp;/g, ' '),
    }}
    tagsStyles={{
      div: {textAlign: props.textAlign, color: props.color},
      li: {color: props.color},
      ul: {marginTop: 20, color: props.color},
      del: {textDecorationLine: 'line-through', textDecorationStyle: 'solid'},
    }}
    baseFontStyle={{color: props.color, textAlign: props.textAlign}}
    listsPrefixesRenderers={{
      ul: (_htmlAttribs, _children, _convertedCSSStyles, passProps) => (
        <View style={[styles.bullet, {backgroundColor: props.color}]} />
      ),
      ol: (_htmlAttribs, _children, _convertedCSSStyles, passProps) => (
        <Text style={[styles.textStyle, {color: props.color}]}>{passProps?.index + 1}.</Text>
      ),
    }}
    renderers={{
      blockquote: (_htmlAttribs, _children, _convertedCSSStyles, passProps) => (
        <Text style={[styles.textStyle, {color: props.color}]}>"{_children}"</Text>
      ),
    }}
    onLinkPress={props.onLinkPress}
  />
)

const styles = StyleSheet.create({
  bullet: {
    marginRight: 5,
    width: 6,
    height: 6,
    marginTop: 5,
    borderRadius: 3,
  },
  textStyle: {
    marginRight: 5,
    fontSize: responsiveFont(14),
  },
})

export default Html
