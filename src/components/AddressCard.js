import React from 'react'
import {View, StyleSheet, Text} from 'react-native'
import Styles from '../Themes/Styles'
import {responsiveHeight, responsiveWidth} from '../Themes/Metrics'
import {TouchableCmp} from './UtilityFunctions'
import FastImage from 'react-native-fast-image'
import Colors from '../Themes/Colors'

const AddressCard = (props) => {
  return (
    <TouchableCmp onPress={props.onPress}>
      <View style={styles.container}>
        <FastImage source={{uri: props.imgSource || ''}} style={styles.image} />
        <View style={styles.textWrapper}>
          <Text style={{...Styles.mediumCapText, ...{alignSelf: 'flex-start', color: Colors.location.title}}}>
            {props.venueName}
          </Text>
          <Text style={{...Styles.xSmallNormalText, ...styles.address}}>{props.venueAddress}</Text>
        </View>
      </View>
    </TouchableCmp>
  )
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    flexDirection: 'row',
    marginBottom: responsiveHeight(10),
    alignItems: 'center',
  },
  image: {
    width: responsiveWidth(100),
    aspectRatio: 1,
  },
  textWrapper: {
    flex: 1,
    height: '100%',
    paddingVertical: responsiveHeight(5),
    paddingHorizontal: responsiveWidth(15),
  },
  address: {
    textAlign: 'left',
    marginTop: responsiveHeight(10),
    lineHeight: responsiveHeight(20),
    color: Colors.location.address,
  },
})

export default AddressCard
