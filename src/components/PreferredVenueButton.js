import React from 'react'
import {View, Text, StyleSheet} from 'react-native'
import Colors from '../Themes/Colors'
import Styles from '../Themes/Styles'
import {TouchableCmp} from './../components/UtilityFunctions'
import {responsiveHeight, responsiveFont, responsiveWidth} from '../Themes/Metrics'
import FontAwesome from 'react-native-vector-icons/FontAwesome'

const PreferredVenueButton = (props) => {
  return (
    <View style={{...styles.preferredVenueBarContainer, ...props.style}}>
      <TouchableCmp activeOpacity={0.6} style={{flex: 1}} onPress={props.onPress}>
        <View style={styles.preferredVenueBar}>
          <View>
            <Text style={{...Styles.smallCapText, ...{color: Colors.home.venueTitle}}}>
              {!props.venueName ? 'Venue name' : props.venueName}
            </Text>
            <Text style={[Styles.xSmallNormalText, {color: Colors.home.venueDesc}]}>
              {!props.ifOnlineOrdering || props.ifOnlineOrdering === undefined
                ? "Online ordering isn't available at this venue"
                : 'Online ordering is available at this venue'}
            </Text>
          </View>
          <View style={{position: 'absolute', right: responsiveWidth(5)}}>
            <FontAwesome
              name={props.icon === 'down' ? 'chevron-circle-down' : 'chevron-circle-up'}
              size={responsiveFont(22)}
              color={Colors.home.preferredVenueButtonIcon}
            />
          </View>
        </View>
      </TouchableCmp>
    </View>
  )
}

const styles = StyleSheet.create({
  preferredVenueBarContainer: {
    width: '100%',
    height: responsiveHeight(55),
    backgroundColor: Colors.first,
  },
  preferredVenueBar: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
})

export default PreferredVenueButton
