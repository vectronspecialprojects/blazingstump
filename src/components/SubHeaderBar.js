import React from 'react'
import {View, Text, StyleSheet} from 'react-native'
import {TouchableCmp} from './UtilityFunctions'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import {responsiveFont, responsiveHeight, responsiveWidth} from '../Themes/Metrics'
import Styles from '../Themes/Styles'
import Colors from '../Themes/Colors'
import {isMultiVenue} from '../constants/env'

const SubHeaderBar = (props) => {
  const filterBtn = props.filterBtn

  return (
    <View style={{...styles.container, ...props.style}}>
      <Text style={{...Styles.mediumBoldText, ...{color: Colors.pageTitle}}}>{props.title}</Text>
      {filterBtn && isMultiVenue && (
        <View style={styles.iconContainer}>
          <TouchableCmp onPress={props.onFilterPress} style={{flex: 1}}>
            <View style={styles.wrapper}>
              <FontAwesome5
                name="filter"
                size={responsiveFont(15)}
                color={Colors.defaultFilterIcon}
                activeOpacity={0.6}
              />
            </View>
          </TouchableCmp>
        </View>
      )}
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: responsiveHeight(44),
    flexDirection: 'row',
    backgroundColor: Colors.subHeaderBackground,
    justifyContent: 'center',
    alignItems: 'center',
  },
  iconContainer: {
    position: 'absolute',
    right: responsiveWidth(15),
    borderRadius: 5,
    borderWidth: 0,
    borderColor: Colors.second,
    overflow: 'hidden',
  },
  wrapper: {
    padding: 5,
  },
})

export default SubHeaderBar
