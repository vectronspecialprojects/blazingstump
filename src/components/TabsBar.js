import React from 'react'
import {View, Text, StyleSheet} from 'react-native'
import {TouchableCmp} from './UtilityFunctions'
import Colors from '../Themes/Colors'
import Fonts from '../Themes/Fonts'
import {shadow} from '../Themes/Metrics'

const TabsBar = (props) => {
  const pages = props.pages

  const Tabs = (props) => {
    return (
      <View
        style={{
          backgroundColor:
            props.selectedPageId === props.page.listing_type_id
              ? Colors.whaton.tabBackgroundActive
              : Colors.whaton.tabBackgroundInactive,
          flex: 1,
        }}>
        <TouchableCmp
          activeOpacity={0.6}
          style={{flex: 1}}
          onPress={props.onPress.bind(this, props.page.listing_type_id)}>
          <View style={styles.textContainer}>
            <Text
              style={{
                ...(props.selectedPageId === props.page.listing_type_id
                  ? {color: Colors.whaton.tabTitleActive}
                  : {color: Colors.whaton.tabTitleInactive}),
                ...styles.title,
              }}
              numberOfLines={1}>
              {props.page.title}
            </Text>
          </View>
        </TouchableCmp>
      </View>
    )
  }

  return (
    <View style={[styles.bar, props.isShadow && {...shadow}]}>
      {pages.map((page) => {
        return (
          <Tabs
            selectedPageId={props.selectedPageId}
            onPress={props.onPress}
            page={page}
            key={page.listing_type_id}
          />
        )
      })}
    </View>
  )
}

const styles = StyleSheet.create({
  bar: {
    width: '100%',
    flexDirection: 'row',
    height: 44,
    backgroundColor: Colors.defaultBackground,
  },
  textContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontFamily: Fonts.openSansBold,
    fontSize: 14,
  },
})

export default TabsBar
