import {Text, StyleSheet, View} from 'react-native'
import React from 'react'
import {TouchableCmp} from './UtilityFunctions'
import Colors from '../Themes/Colors'
import {responsiveFont, responsiveWidth} from '../Themes/Metrics'
import fonts from '../Themes/Fonts'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

export default function RadioButton({
  value,
  onPress,
  size = 20,
  style,
  tintColor = Colors.second,
  title,
  titleStyle,
  disabled = false,
}) {
  return (
    <TouchableCmp
      disabled={disabled}
      onPress={() => {
        onPress()
      }}>
      <View style={[styles.container, style]}>
        <MaterialIcons
          name={value ? 'radio-button-checked' : 'radio-button-unchecked'}
          size={size}
          color={value ? tintColor : Colors.white}
        />
        <Text style={[styles.title, titleStyle]}>
          {title}
        </Text>
      </View>
    </TouchableCmp>
  )
}
const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  title: {
    fontSize: responsiveFont(13),
    marginLeft: responsiveWidth(5),
    fontFamily: fonts.openSans,
    color: Colors.white,
    flexShrink: 1,
  },
})
