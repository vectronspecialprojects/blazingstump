import React from 'react'
import {View, StyleSheet, Text} from 'react-native'
import Barcode from 'react-native-barcode-builder'
import Colors from '../Themes/Colors'
import Styles from '../Themes/Styles'
import {responsiveHeight} from '../Themes/Metrics'

const BarcodeBar = (props) => {
  return (
    <View>
      <View style={{...styles.barcodeContainer, ...props.style}}>
        <Barcode
          value={props.value}
          format="CODE128B"
          width={props.width}
          lineColor={Colors.black}
          background={Colors.white}
        />
      </View>
      <Text style={Styles.xSmallNormalText}>{props.title.concat(' ', props.text)}</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  barcodeContainer: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    height: responsiveHeight(42),
    overflow: 'hidden',
  },
})

export default BarcodeBar
