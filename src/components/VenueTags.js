import React, {useRef, useState} from 'react'
import {View, Text, FlatList, StyleSheet} from 'react-native'
import {responsiveHeight, responsiveWidth} from '../Themes/Metrics'
import {TouchableCmp} from './UtilityFunctions'
import Colors from '../Themes/Colors'
import Styles from '../Themes/Styles'

const VenueTags = (props) => {
  const listRef = useRef()

  const renderItem = (itemData) => {
    const tag = itemData.item
    return (
      <View
        style={[
          styles.tags,
          props.selectedTag === tag.id && {backgroundColor: Colors.location.tagsBackgroundActive},
        ]}>
        <TouchableCmp
          style={{flex: 1}}
          onPress={() => {
            props.onPress(tag.id)
            listRef.current?.scrollToIndex({index: itemData.index, animated: true, viewPosition: 0.5})
          }}>
          <View style={styles.textWrapper}>
            <Text
              style={[
                Styles.smallCapBoldText,
                props.selectedTag === tag.id
                  ? {color: Colors.location.tagsTitleActive}
                  : {color: Colors.location.tagsTitleInactive},
              ]}>
              {tag.name}
            </Text>
          </View>
        </TouchableCmp>
      </View>
    )
  }
  return (
    <View>
      <FlatList
        ref={listRef}
        data={props.data}
        style={{
          height: responsiveHeight(50),
          paddingVertical: responsiveHeight(8),
          backgroundColor: Colors.first,
        }}
        showsHorizontalScrollIndicator={false}
        renderItem={renderItem}
        horizontal={true}
        keyExtractor={(item, index) => index.toString()}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  tags: {
    marginHorizontal: responsiveWidth(5),
    paddingHorizontal: responsiveWidth(10),
    borderRadius: 12,
    backgroundColor: Colors.location.tagsBackgroundInactive,
    overflow: 'hidden',
  },
  textWrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
})

export default VenueTags
