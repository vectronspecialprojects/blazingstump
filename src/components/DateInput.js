import React, {useState, useEffect} from 'react'
import {View, Text, StyleSheet} from 'react-native'
import {responsiveHeight, responsiveWidth} from '../Themes/Metrics'
import DateTime from './DateTime'
import Colors from '../Themes/Colors'
import Fonts from '../Themes/Fonts'
import moment from 'moment'

const DateInput = (props) => {
  const { onInputChange, id, style, styleText, maxDate, minDate } = props
  const [isValid, setIsValid] = useState(true)
  const [date, setDate] = useState('')

  const dateChangeHandler = (date) => {
    if (props.required && moment().diff(date, 'years') >= 18) {
      setIsValid(true)
    } else {
      setIsValid(false)
    }
    setDate(date)
  }

  useEffect(() => {
    onInputChange(id, moment(date).format('DD/MM/YYYY'), isValid)
  }, [onInputChange, id, date, isValid])

  return (
    <View style={styles.formControl}>
      <Text style={[styles.label, styleText]}>{props.label}</Text>
      <DateTime
        pickerStyle={[styles.pickerStyle, style]}
        containerStyle={[styles.dateWrapper, style]}
        value={date}
        onChange={dateChangeHandler}
        maxDate={maxDate}
        minDate={minDate}
      />
      {!isValid && (
        <View style={styles.errorContainer}>
          <Text style={styles.errorText}>{props.errorText}</Text>
        </View>
      )}
    </View>
  )
}

const styles = StyleSheet.create({
  formControl: {
    width: '100%',
  },
  label: {
    fontFamily: Fonts.openSansBold,
    marginVertical: responsiveHeight(4),
    color: Colors.defaultTextColor,
  },
  dateWrapper: {
    marginHorizontal: 0,
    paddingHorizontal: responsiveWidth(5),
    paddingVertical: responsiveHeight(5),
    backgroundColor: Colors.first,
    height: responsiveHeight(48),
    borderRadius: 15,
    justifyContent: 'center',
  },
  pickerStyle: {
    marginLeft: 0,
    width: '100%',
    height: '100%',
    color: 'white',
  },
  errorContainer: {
    marginVertical: responsiveHeight(5),
  },
  errorText: {
    fontFamily: Fonts.openSans,
    color: 'red',
    fontSize: 13,
  },
})

export default DateInput
