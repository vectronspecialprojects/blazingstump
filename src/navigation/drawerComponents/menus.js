import RouteKey from '../RouteKey'
import {venueWebsite} from '../../constants/env'

export const menus = [
  {
    id: 1,
    route: RouteKey.FrontpageNavigator,
    name: 'Home',
    icon: 'home',
    param: {page_name: 'Home'},
  },
  {
    id: 2,
    route: RouteKey.AboutNavigator,
    name: 'About',
    icon: 'star',
    param: {page_name: 'About'},
  },
  {
    id: 3,
    route: RouteKey.LegalsNavigator,
    name: 'Legals',
    icon: 'book',
    param: {page_name: 'Legals'},
  },
  {
    id: 4,
    route: RouteKey.LocationsNavigator,
    name: 'Locations',
    icon: 'map-marker-alt',
    param: {page_name: 'Locations'},
  },
  {
    id: 5,
    route: RouteKey.WebviewNavigator,
    name: 'Website',
    icon: 'globe',
    param: {special_link: venueWebsite},
  },
]
