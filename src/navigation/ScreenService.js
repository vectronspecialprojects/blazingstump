import RouteKey from './RouteKey'
import LoginScreen, {screenOptions as loginScreenOption} from '../screens/AuthComponent/LoginScreen'
import SignupScreen, {screenOptions as signupScreenOptions} from '../screens/AuthComponent/SignupScreen'
import ForgotPasswordScreen, {
  screenOptions as forgotPasswordScreenOptions,
} from '../screens/AuthComponent/ForgotPasswordScreen'
import PreferredVenueScreen, {
  screenOptions as preferredVenueScreenOptions,
} from '../screens/PreferredVenueComponent/PreferredVenueScreen'
import MapScreen, {screenOptions as mapScreenOptions} from '../screens/MapScreen'
import ProfileScreen, {screenOptions as profileScreenOptions} from '../screens/ProfileScreen'
import HistoryScreen, {screenOptions as historyScreenOptions} from '../screens/HistoryScreen'
import FavoriteScreen, {screenOptions as favoriteScreenOptions} from '../screens/FavoriteScreen'
import StartupScreen, {screenOptions as startupScreenOptions} from '../screens/StartupScreen'
import AboutScreen, {screenOptions as aboutScreenOptions} from '../screens/AboutScreen'
import WhatsonScreen, {screenOptions as whatsonScreenOptions} from '../screens/WhatsonScreen'
import WhatsonDetailsScreen, {
  screenOptions as whatsonDetailsScreenOptions,
} from '../screens/WhatsonDetailsScreen'
import ReferScreen, {screenOptions as referScreenOptions} from '../screens/ReferScreen'
import TicketsScreen, {screenOptions as ticketsScreenOptions} from '../screens/TicketComponent/TicketsScreen'
import GiftCertificateScreen, {
  screenOptions as giftCertificateScreenOptions,
} from '../screens/GiftCertificateScreen'
import SurveysScreen, {screenOptions as surveysScreenOptions} from '../screens/SurveyComponent/SurveysScreen'
import LocationsScreen, {screenOptions as locationsScreenOptions} from '../screens/LocationsScreen'
import StampcardScreen, {screenOptions as stampcardScreenOptions} from '../screens/StampcardScreen'
import FaqScreen, {screenOptions as faqScreenOptions} from '../screens/FaqScreen'
import FeedbackScreen, {screenOptions as feedbackScreenOptions} from '../screens/FeedbackScreen'
import VouchersScreen, {screenOptions as vouchersScreenOptions} from '../screens/VouchersScreen'
import VoucherDetailScreen, {
  screenOptions as voucherDetailScreenOptions,
} from '../screens/VoucherDetailScreen'
import OffersScreen, {screenOptions as offersScreenOptions} from '../screens/OffersScreen'
import EnquiriesScreen, {screenOptions as enquiriesScreenOptions} from '../screens/EnquiriesScreen'
import LegalsScreen, {screenOptions as legalsScreenOptions} from '../screens/LegalsScreen'
import HomeScreen, {screenOptions as homeScreenOptions} from '../screens/HomeComponent/HomeScreen'
import WebviewScreen, {screenOptions as webviewScreenOptions} from '../screens/WebviewScreen'
import YourorderScreen, {screenOptions as yourorderScreenOptions} from '../screens/YourorderScreen'
import ResetPasswordScreen, {
  screenOptions as resetPasswordScreen,
} from '../screens/ProfileComponent/ResetPasswordScreen'
import ChangeEmailScreen, {
  screenOptions as changeEmailScreen,
} from '../screens/ProfileComponent/ChangeEmailScreen'
import SurveyDetailsScreen, {
  screenOptions as surveyDetailScreen,
} from '../screens/SurveyComponent/SurveyDetailsScreen'
import OurTeamScreen, {screenOptions as ourTeamScreenOptions} from '../screens/OurTeamComponent/OurTeamScreen'
import FavoriteDetailScreen, {
  screenOptions as favoriteDetailScreen,
} from '../screens/ProfileComponent/FavoriteDetailScreen'
import TicketDetailScreen, {
  screenOptions as ticketDetailScreen,
} from '../screens/TicketComponent/TicketDetailScreen'
import ProductListScreen, {
  screenOptions as productListScreen,
} from '../screens/ProductComponent/ProductListScreen'
import BuyGiftCertificateScreen, {
  screenOptions as buyGiftCertificationScreen,
} from '../screens/BuyGiftCertificateScreen'
import MatchAccountScreen, {
  screenOptions as matchAccountScreenOptions,
} from '../screens/AuthComponent/MatchAccountScreen'
import MatchAccountInfo, {
  screenOptions as matchAccountInfoScreenOptions,
} from '../screens/AuthComponent/MatchAccountInfo'

export const screenMatch = (screen) => {
  switch (screen) {
    case RouteKey.LoginScreen:
      return LoginScreen
    case RouteKey.ForgotPasswordScreen:
      return ForgotPasswordScreen
    case RouteKey.SignupScreen:
      return SignupScreen
    case RouteKey.PreferredVenueScreen:
      return PreferredVenueScreen
    case RouteKey.MapScreen:
      return MapScreen
    case RouteKey.ProfileScreen:
      return ProfileScreen
    case RouteKey.HistoryScreen:
      return HistoryScreen
    case RouteKey.FavoriteScreen:
      return FavoriteScreen
    case RouteKey.StartupScreen:
      return StartupScreen
    case RouteKey.AboutScreen:
      return AboutScreen
    case RouteKey.WhatsonScreen:
      return WhatsonScreen
    case RouteKey.WhatsonDetailsScreen:
      return WhatsonDetailsScreen
    case RouteKey.ReferScreen:
      return ReferScreen
    case RouteKey.TicketsScreen:
      return TicketsScreen
    case RouteKey.GiftCertificateScreen:
      return GiftCertificateScreen
    case RouteKey.SurveysScreen:
      return SurveysScreen
    case RouteKey.LocationsScreen:
      return LocationsScreen
    case RouteKey.StampcardScreen:
      return StampcardScreen
    case RouteKey.FaqScreen:
      return FaqScreen
    case RouteKey.FeedbackScreen:
      return FeedbackScreen
    case RouteKey.VouchersScreen:
      return VouchersScreen
    case RouteKey.VoucherDetailScreen:
      return VoucherDetailScreen
    case RouteKey.OffersScreen:
      return OffersScreen
    case RouteKey.EnquiriesScreen:
      return EnquiriesScreen
    case RouteKey.LegalsScreen:
      return LegalsScreen
    case RouteKey.HomeScreen:
      return HomeScreen
    case RouteKey.WebviewScreen:
      return WebviewScreen
    case RouteKey.YourorderScreen:
      return YourorderScreen
    case RouteKey.ResetPasswordScreen:
      return ResetPasswordScreen
    case RouteKey.ChangeEmailScreen:
      return ChangeEmailScreen
    case RouteKey.SurveyDetailsScreen:
      return SurveyDetailsScreen
    case RouteKey.OurTeamScreen:
      return OurTeamScreen
    case RouteKey.FavoriteDetailScreen:
      return FavoriteDetailScreen
    case RouteKey.TicketDetailScreen:
      return TicketDetailScreen
    case RouteKey.ProductListScreen:
      return ProductListScreen
    case RouteKey.BuyGiftCertificateScreen:
      return BuyGiftCertificateScreen
    case RouteKey.MatchAccountScreen:
      return MatchAccountScreen
    case RouteKey.MatchAccountInfo:
      return MatchAccountInfo
  }
}

export const optionsMatch = (screen) => {
  switch (screen) {
    case RouteKey.PreferredVenueScreen:
      return preferredVenueScreenOptions
    case RouteKey.MapScreen:
      return mapScreenOptions
    case RouteKey.ProfileScreen:
      return profileScreenOptions
    case RouteKey.HistoryScreen:
      return historyScreenOptions
    case RouteKey.FavoriteScreen:
      return favoriteScreenOptions
    case RouteKey.StartupScreen:
      return startupScreenOptions
    case RouteKey.AboutScreen:
      return aboutScreenOptions
    case RouteKey.WhatsonScreen:
      return whatsonScreenOptions
    case RouteKey.WhatsonDetailsScreen:
      return whatsonDetailsScreenOptions
    case RouteKey.ReferScreen:
      return referScreenOptions
    case RouteKey.TicketsScreen:
      return ticketsScreenOptions
    case RouteKey.GiftCertificateScreen:
      return giftCertificateScreenOptions
    case RouteKey.SurveysScreen:
      return surveysScreenOptions
    case RouteKey.LocationsScreen:
      return locationsScreenOptions
    case RouteKey.StampcardScreen:
      return stampcardScreenOptions
    case RouteKey.FaqScreen:
      return faqScreenOptions
    case RouteKey.FeedbackScreen:
      return feedbackScreenOptions
    case RouteKey.VouchersScreen:
      return vouchersScreenOptions
    case RouteKey.VoucherDetailScreen:
      return voucherDetailScreenOptions
    case RouteKey.OffersScreen:
      return offersScreenOptions
    case RouteKey.EnquiriesScreen:
      return enquiriesScreenOptions
    case RouteKey.LegalsScreen:
      return legalsScreenOptions
    case RouteKey.HomeScreen:
      return homeScreenOptions
    case RouteKey.WebviewScreen:
      return webviewScreenOptions
    case RouteKey.ResetPasswordScreen:
      return resetPasswordScreen
    case RouteKey.ChangeEmailScreen:
      return changeEmailScreen
    case RouteKey.SurveyDetailsScreen:
      return surveyDetailScreen
    case RouteKey.OurTeamScreen:
      return ourTeamScreenOptions
    case RouteKey.FavoriteDetailScreen:
      return favoriteDetailScreen
    case RouteKey.TicketDetailScreen:
      return ticketDetailScreen
    case RouteKey.ProductListScreen:
      return productListScreen
    case RouteKey.BuyGiftCertificateScreen:
      return buyGiftCertificationScreen
    case RouteKey.YourorderScreen:
      return yourorderScreenOptions
    case RouteKey.SignupScreen:
      return signupScreenOptions
    case RouteKey.LoginScreen:
      return loginScreenOption
    case RouteKey.ForgotPasswordScreen:
      return forgotPasswordScreenOptions
    case RouteKey.MatchAccountScreen:
      return matchAccountScreenOptions
    case RouteKey.MatchAccountInfo:
      return matchAccountInfoScreenOptions
  }
}
