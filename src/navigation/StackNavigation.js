import React from 'react'
import {createStackNavigator} from '@react-navigation/stack'
import {screenMatch, optionsMatch} from './ScreenService'
import {defaultStackOptions} from './DefaultOptions'
import {useSelector} from 'react-redux'
import RouteKey from './RouteKey'

export const componentMatch = (stackName) => {
  switch (stackName) {
    case RouteKey.HomeNavigator:
      return HomeNavigator
    case RouteKey.WebviewNavigator:
      return WebviewNavigator
    case RouteKey.YourorderNavigator:
      return YourorderNavigator
    case RouteKey.WhatsonNavigator:
      return WhatsonNavigator
    case RouteKey.TicketsNavigator:
      return TicketsNavigator
    case RouteKey.ReferNavigator:
      return ReferNavigator
    case RouteKey.EnquiriesNavigator:
      return EnquiriesNavigator
    case RouteKey.SurveysNavigator:
      return SurveysNavigator
    case RouteKey.FaqNavigator:
      return FaqNavigator
    case RouteKey.LocationsNavigator:
      return LocationsNavigator
    case RouteKey.StampcardNavigator:
      return StampcardNavigator
    case RouteKey.OffersNavigator:
      return OffersNavigator
    case RouteKey.FeedbackNavigator:
      return FeedbackNavigator
    case RouteKey.AboutNavigator:
      return AboutNavigator
    case RouteKey.LegalsNavigator:
      return LegalsNavigator
    case RouteKey.VouchersNavigator:
      return VouchersNavigator
    case RouteKey.GiftCertificateNavigator:
      return GiftCertificateNavigator
    case RouteKey.FrontpageNavigator:
      return FrontpageNavigator
    case RouteKey.OurTeamNavigator:
      return OurTeamNavigator
    default:
      return HomeNavigator
  }
}

const Stack = createStackNavigator()

const WebviewNavigator = (props) => {
  return (
    <Stack.Navigator screenOptions={defaultStackOptions}>
      <Stack.Screen
        name={RouteKey.WebviewScreen}
        component={screenMatch(RouteKey.WebviewScreen)}
        options={optionsMatch(RouteKey.WebviewScreen)}
        initialParams={{params: props.route.params.params}}
      />
    </Stack.Navigator>
  )
}

const YourorderNavigator = (props) => {
  return (
    <Stack.Navigator screenOptions={defaultStackOptions}>
      <Stack.Screen
        name={RouteKey.YourorderScreen}
        component={screenMatch(RouteKey.YourorderScreen)}
        options={optionsMatch(RouteKey.YourorderScreen)}
        initialParams={{params: props.route.params.params}}
      />
    </Stack.Navigator>
  )
}

const FrontpageNavigator = (props) => {
  return (
    <Stack.Navigator screenOptions={defaultStackOptions}>
      <Stack.Screen
        name={RouteKey.StartupScreen}
        component={screenMatch(RouteKey.StartupScreen)}
        options={optionsMatch(RouteKey.StartupScreen)}
      />
      <Stack.Screen
        name={RouteKey.LoginScreen}
        component={screenMatch(RouteKey.LoginScreen)}
        options={optionsMatch(RouteKey.LoginScreen)}
      />
      <Stack.Screen
        name={RouteKey.SignupScreen}
        component={screenMatch(RouteKey.SignupScreen)}
        options={optionsMatch(RouteKey.SignupScreen)}
      />
      <Stack.Screen
        name={RouteKey.ForgotPasswordScreen}
        component={screenMatch(RouteKey.ForgotPasswordScreen)}
        options={optionsMatch(RouteKey.ForgotPasswordScreen)}
      />
      <Stack.Screen
        name={RouteKey.MatchAccountScreen}
        component={screenMatch(RouteKey.MatchAccountScreen)}
        options={optionsMatch(RouteKey.MatchAccountScreen)}
      />
      <Stack.Screen
        name={RouteKey.MatchAccountInfo}
        component={screenMatch(RouteKey.MatchAccountInfo)}
        options={optionsMatch(RouteKey.MatchAccountInfo)}
      />
    </Stack.Navigator>
  )
}

const VouchersNavigator = (props) => {
  return (
    <Stack.Navigator screenOptions={defaultStackOptions}>
      <Stack.Screen
        name={RouteKey.VouchersScreen}
        component={screenMatch(RouteKey.VouchersScreen)}
        options={optionsMatch(RouteKey.VouchersScreen)}
        initialParams={{params: props.route.params.params}}
      />
      <Stack.Screen
        name={RouteKey.VoucherDetailScreen}
        component={screenMatch(RouteKey.VoucherDetailScreen)}
        options={optionsMatch(RouteKey.VoucherDetailScreen)}
      />
    </Stack.Navigator>
  )
}

const WhatsonNavigator = (props) => {
  return (
    <Stack.Navigator screenOptions={defaultStackOptions}>
      <Stack.Screen
        name={RouteKey.WhatsonScreen}
        component={screenMatch(RouteKey.WhatsonScreen)}
        options={optionsMatch(RouteKey.WhatsonScreen)}
        initialParams={{params: props.route.params.params}}
      />
      <Stack.Screen
        name={RouteKey.WhatsonDetailsScreen}
        component={screenMatch(RouteKey.WhatsonDetailsScreen)}
        options={optionsMatch(RouteKey.WhatsonDetailsScreen)}
      />
      <Stack.Screen
        name={RouteKey.ProductListScreen}
        component={screenMatch(RouteKey.ProductListScreen)}
        options={optionsMatch(RouteKey.ProductListScreen)}
      />
    </Stack.Navigator>
  )
}

const TicketsNavigator = (props) => {
  return (
    <Stack.Navigator screenOptions={defaultStackOptions}>
      <Stack.Screen
        name={RouteKey.TicketsScreen}
        component={screenMatch(RouteKey.TicketsScreen)}
        options={optionsMatch(RouteKey.TicketsScreen)}
        initialParams={{params: props.route.params.params}}
      />
      <Stack.Screen
        name={RouteKey.TicketDetailScreen}
        component={screenMatch(RouteKey.TicketDetailScreen)}
        options={optionsMatch(RouteKey.TicketDetailScreen)}
      />
    </Stack.Navigator>
  )
}

const OurTeamNavigator = (props) => {
  return (
    <Stack.Navigator screenOptions={defaultStackOptions}>
      <Stack.Screen
        name={RouteKey.OurTeamScreen}
        component={screenMatch(RouteKey.OurTeamScreen)}
        options={optionsMatch(RouteKey.OurTeamScreen)}
        initialParams={{params: props.route.params.params}}
      />
    </Stack.Navigator>
  )
}

const GiftCertificateNavigator = (props) => {
  return (
    <Stack.Navigator screenOptions={defaultStackOptions}>
      <Stack.Screen
        name={RouteKey.GiftCertificateScreen}
        component={screenMatch(RouteKey.GiftCertificateScreen)}
        options={optionsMatch(RouteKey.GiftCertificateScreen)}
        initialParams={{params: props.route.params.params}}
      />
      <Stack.Screen
        name={RouteKey.BuyGiftCertificateScreen}
        component={screenMatch(RouteKey.BuyGiftCertificateScreen)}
        options={optionsMatch(RouteKey.BuyGiftCertificateScreen)}
        initialParams={{params: props.route.params.params}}
      />
    </Stack.Navigator>
  )
}

const ReferNavigator = (props) => {
  return (
    <Stack.Navigator screenOptions={defaultStackOptions}>
      <Stack.Screen
        name={RouteKey.ReferScreen}
        component={screenMatch(RouteKey.ReferScreen)}
        options={optionsMatch(RouteKey.ReferScreen)}
        initialParams={{params: props.route.params.params}}
      />
    </Stack.Navigator>
  )
}

const EnquiriesNavigator = (props) => {
  return (
    <Stack.Navigator screenOptions={defaultStackOptions}>
      <Stack.Screen
        name={RouteKey.EnquiriesScreen}
        component={screenMatch(RouteKey.EnquiriesScreen)}
        options={optionsMatch(RouteKey.EnquiriesScreen)}
        initialParams={{params: props.route.params.params}}
      />
    </Stack.Navigator>
  )
}

const SurveysNavigator = (props) => {
  return (
    <Stack.Navigator screenOptions={defaultStackOptions}>
      <Stack.Screen
        name={RouteKey.SurveysScreen}
        component={screenMatch(RouteKey.SurveysScreen)}
        options={optionsMatch(RouteKey.SurveysScreen)}
        initialParams={{params: props.route.params.params}}
      />
      <Stack.Screen
        name={RouteKey.SurveyDetailsScreen}
        component={screenMatch(RouteKey.SurveyDetailsScreen)}
        options={optionsMatch(RouteKey.SurveyDetailsScreen)}
      />
    </Stack.Navigator>
  )
}

const FaqNavigator = (props) => {
  return (
    <Stack.Navigator screenOptions={defaultStackOptions}>
      <Stack.Screen
        name={RouteKey.FaqScreen}
        component={screenMatch(RouteKey.FaqScreen)}
        options={optionsMatch(RouteKey.FaqScreen)}
        initialParams={{params: props.route.params.params}}
      />
    </Stack.Navigator>
  )
}

const FeedbackNavigator = (props) => {
  return (
    <Stack.Navigator screenOptions={defaultStackOptions}>
      <Stack.Screen
        name={RouteKey.FeedbackScreen}
        component={screenMatch(RouteKey.FeedbackScreen)}
        options={optionsMatch(RouteKey.FeedbackScreen)}
        initialParams={{params: props.route.params.params}}
      />
    </Stack.Navigator>
  )
}

const LocationsNavigator = (props) => {
  return (
    <Stack.Navigator screenOptions={defaultStackOptions}>
      <Stack.Screen
        name={RouteKey.LocationsScreen}
        component={screenMatch(RouteKey.LocationsScreen)}
        options={optionsMatch(RouteKey.LocationsScreen)}
        initialParams={{params: props.route.params.params}}
      />
      <Stack.Screen
        name={RouteKey.MapScreen}
        component={screenMatch(RouteKey.MapScreen)}
        options={optionsMatch(RouteKey.MapScreen)}
      />
      <Stack.Screen
        name={RouteKey.WebviewScreen}
        component={screenMatch(RouteKey.WebviewScreen)}
        options={optionsMatch(RouteKey.WebviewScreen)}
      />
    </Stack.Navigator>
  )
}

const StampcardNavigator = (props) => {
  return (
    <Stack.Navigator screenOptions={defaultStackOptions}>
      <Stack.Screen
        name={RouteKey.StampcardScreen}
        component={screenMatch(RouteKey.StampcardScreen)}
        options={optionsMatch(RouteKey.StampcardScreen)}
        initialParams={{params: props.route.params.params}}
      />
      <Stack.Screen
        name={RouteKey.VoucherDetailScreen}
        component={screenMatch(RouteKey.VoucherDetailScreen)}
        options={optionsMatch(RouteKey.VoucherDetailScreen)}
      />
    </Stack.Navigator>
  )
}

const AboutNavigator = (props) => {
  return (
    <Stack.Navigator screenOptions={defaultStackOptions}>
      <Stack.Screen
        name={RouteKey.AboutScreen}
        component={screenMatch(RouteKey.AboutScreen)}
        options={optionsMatch(RouteKey.AboutScreen)}
        initialParams={{params: props.route.params.params}}
      />
    </Stack.Navigator>
  )
}

const LegalsNavigator = (props) => {
  return (
    <Stack.Navigator screenOptions={defaultStackOptions}>
      <Stack.Screen
        name={RouteKey.LegalsScreen}
        component={screenMatch(RouteKey.LegalsScreen)}
        options={optionsMatch(RouteKey.LegalsScreen)}
        initialParams={{params: props.route.params.params}}
      />
    </Stack.Navigator>
  )
}

const OffersNavigator = (props) => {
  return (
    <Stack.Navigator screenOptions={defaultStackOptions}>
      <Stack.Screen
        name={RouteKey.OffersScreen}
        component={screenMatch(RouteKey.OffersScreen)}
        options={optionsMatch(RouteKey.OffersScreen)}
        initialParams={{params: props.route.params.params}}
      />
    </Stack.Navigator>
  )
}

export const HomeNavigator = (props) => {
  const homeMenus = useSelector((state) => state.app.homeMenus)
  console.log ('homeMenus', homeMenus)
  return (
    <Stack.Navigator screenOptions={defaultStackOptions}>
      <Stack.Screen
        name="Home"
        component={screenMatch(RouteKey.HomeScreen)}
        options={optionsMatch(RouteKey.HomeScreen)}
      />
      <Stack.Screen
        name={RouteKey.PreferredVenueScreen}
        component={screenMatch(RouteKey.PreferredVenueScreen)}
        options={optionsMatch(RouteKey.PreferredVenueScreen)}
      />
      <Stack.Screen
        name={RouteKey.ProfileScreen}
        component={screenMatch(RouteKey.ProfileScreen)}
        options={optionsMatch(RouteKey.ProfileScreen)}
      />
      <Stack.Screen
        name={RouteKey.HistoryScreen}
        component={screenMatch(RouteKey.HistoryScreen)}
        options={optionsMatch(RouteKey.HistoryScreen)}
      />
      <Stack.Screen
        name={RouteKey.FavoriteScreen}
        component={screenMatch(RouteKey.FavoriteScreen)}
        options={optionsMatch(RouteKey.FavoriteScreen)}
      />
      <Stack.Screen
        name={RouteKey.VoucherDetailScreen}
        component={screenMatch(RouteKey.VoucherDetailScreen)}
        options={optionsMatch(RouteKey.VoucherDetailScreen)}
      />
      <Stack.Screen
        name={RouteKey.WhatsonDetailsScreen}
        component={screenMatch(RouteKey.WhatsonDetailsScreen)}
        options={optionsMatch(RouteKey.WhatsonDetailsScreen)}
      />
      <Stack.Screen
        name={RouteKey.ProductListScreen}
        component={screenMatch(RouteKey.ProductListScreen)}
        options={optionsMatch(RouteKey.ProductListScreen)}
      />
      <Stack.Screen
        name={RouteKey.MapScreen}
        component={screenMatch(RouteKey.MapScreen)}
        options={optionsMatch(RouteKey.MapScreen)}
      />
      <Stack.Screen
        name={RouteKey.ChangeEmailScreen}
        component={screenMatch(RouteKey.ChangeEmailScreen)}
        options={optionsMatch(RouteKey.ChangeEmailScreen)}
      />
      <Stack.Screen
        name={RouteKey.ResetPasswordScreen}
        component={screenMatch(RouteKey.ResetPasswordScreen)}
        options={optionsMatch(RouteKey.ResetPasswordScreen)}
      />
      <Stack.Screen
        name={RouteKey.FavoriteDetailScreen}
        component={screenMatch(RouteKey.FavoriteDetailScreen)}
        options={optionsMatch(RouteKey.FavoriteDetailScreen)}
      />
      <Stack.Screen
        name={RouteKey.WebviewScreen}
        component={screenMatch(RouteKey.WebviewScreen)}
        options={optionsMatch(RouteKey.WebviewScreen)}
      />
      {homeMenus.map((homeMenu) => {
        if (
          homeMenu.icon === '' ||
          homeMenu.state === '' ||
          homeMenu.state === 'none' ||
          (homeMenu.page_layout === 'special_view' &&
            (homeMenu.special_page === 'points' ||
              homeMenu.special_page === 'redeems' ||
              homeMenu.special_page === 'balance' ||
              homeMenu.special_page === 'saved'))
        ) {
          return null
        } else {
          return (
            <Stack.Screen
              key={homeMenu.id}
              name={RouteKey.homeMenu + homeMenu.id}
              component={screenMatch(homeMenu.state)}
              options={optionsMatch(homeMenu.state)}
              initialParams={{params: homeMenu}}
            />
          )
        }
      })}
    </Stack.Navigator>
  )
}
