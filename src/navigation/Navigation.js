import React from 'react'
import {StyleSheet} from 'react-native'
import {useSelector, useDispatch} from 'react-redux'
import {createDrawerNavigator, DrawerItemList} from '@react-navigation/drawer'
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs'
import {componentMatch} from './StackNavigation'
import {DrawerCtnOptions, tabOptions} from './DefaultOptions'
import Colors from '../Themes/Colors'
import * as authServicesActions from '../store/actions/authServices'
import RouteKey from './RouteKey'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import {isIOS} from '../Themes/Metrics'
import {DrawerLogo} from './drawerComponents/DrawerLogo'
import {menus} from './drawerComponents/menus'
import LogOut from './drawerComponents/LogOut'

//tab
const TabStackNavigator = createBottomTabNavigator()
export const TabNavigator = (props) => {
  const tabMenus = useSelector((state) => state.app.tabMenus)
  return (
    <TabStackNavigator.Navigator backBehavior="initialRoute" tabBarOptions={tabOptions}>
      {tabMenus.map((tabMenu) => {
        if (tabMenu.icon === '' || tabMenu.state === '') return null
        return (
          <TabStackNavigator.Screen
            key={tabMenu.id}
            name={tabMenu.id === 1 ? RouteKey.HomeNavigator : RouteKey.tabMenu + tabMenu.id}
            component={componentMatch(tabMenu.state)}
            initialParams={{params: tabMenu}}
            options={{
              tabBarLabel: tabMenu.page_name,
              unmountOnBlur: true,
              tabBarIcon: (props) => (
                <FontAwesome5
                  name={tabMenu.icon}
                  style={{marginTop: isIOS() ? 10 : 0}}
                  size={22}
                  color={props.color}
                />
              ),
            }}
          />
        )
      })}
    </TabStackNavigator.Navigator>
  )
}

//drawer after signin
const DrawerStackAfterAuth = createDrawerNavigator()
export const DrawerAfterAuth = () => {
  const dispatch = useDispatch()
  const drawerMenus = useSelector((state) => state.app.drawerMenus)
  return (
    <DrawerStackAfterAuth.Navigator
      drawerContent={(props) => (
        <DrawerLogo>
          <DrawerItemList {...props} />
          <LogOut
            onPress={() => {
              props.navigation.toggleDrawer()
              dispatch(authServicesActions.logout())
            }}
          />
        </DrawerLogo>
      )}
      drawerContentOptions={DrawerCtnOptions}
      drawerStyle={styles.drawerStyle}>
      <DrawerStackAfterAuth.Screen
        name={RouteKey.Home}
        component={TabNavigator}
        options={{
          drawerLabel: 'Home',
          drawerIcon: (props) => (
            <FontAwesome5 name="home" style={{width: 30}} size={22} color={props.color} />
          ),
        }}
      />

      {drawerMenus.map((drawerMenu) => {
        if (drawerMenu.icon === '') return null
        return (
          <DrawerStackAfterAuth.Screen
            key={drawerMenu.id}
            name={RouteKey.sideMenu + drawerMenu.id}
            component={componentMatch(drawerMenu.state)}
            initialParams={{params: drawerMenu}}
            options={{
              unmountOnBlur: true,
              drawerLabel: drawerMenu.page_name,
              drawerIcon: (props) => (
                <FontAwesome5
                  name={drawerMenu.icon}
                  size={22}
                  style={{width: 30}}
                  color={props.color}
                />
              ),
            }}
          />
        )
      })}
    </DrawerStackAfterAuth.Navigator>
  )
}

//drawer before signin
const DrawerStackBeforeAuth = createDrawerNavigator()
export const DrawerBeforeAuth = () => (
  <DrawerStackBeforeAuth.Navigator
    drawerContent={(props) => (
      <DrawerLogo>
        <DrawerItemList {...props} />
      </DrawerLogo>
    )}
    drawerContentOptions={DrawerCtnOptions}
    drawerStyle={styles.drawerStyle}>
    {menus.map((menu) => (
      <DrawerStackBeforeAuth.Screen
        key={menu.id}
        name={menu.route}
        component={componentMatch(menu.route)}
        initialParams={{params: menu.param}}
        options={{
          drawerLabel: menu.name,
          drawerIcon: (props) => (
            <FontAwesome5 name={menu.icon} size={22} style={{width: 30}} color={props.color} />
          ),
        }}
      />
    ))}
  </DrawerStackBeforeAuth.Navigator>
)

const styles = StyleSheet.create({
  drawerStyle: {
    flex: 1,
    width: '80%',
    backgroundColor: Colors.drawer.background,
  },
})
