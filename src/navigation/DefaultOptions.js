import React from 'react'
import {View, Image, StyleSheet, TouchableOpacity} from 'react-native'
import Colors from '../Themes/Colors'
import Fonts from '../Themes/Fonts'
import {navigate, checkRouteOrigin} from './NavigationService'
import RouteKey from './RouteKey'
import Images from '../Themes/Images'

export const tabOptions = {
  labelStyle: {
    fontFamily: Fonts.openSans,
    fontSize: 10,
  },
  keyboardHidesTabBar: true,
  inactiveTintColor: Colors.tabs.tabLabel,
  activeTintColor: Colors.tabs.activeTabLabel,
  showLabel: true,
  style: {
    backgroundColor: Colors.tabs.tabBackground,
    paddingTop: 0,
  },
}

export const defaultStackOptions = {
  headerStyle: {
    backgroundColor: Colors.first,
  },
  headerTitleStyle: {
    fontFamily: Fonts.openSans,
  },
  headerBackTitleStyle: {
    fontFamily: Fonts.openSans,
  },
  headerBackTitleVisible: false,
  headerTintColor: Colors.defaultTextColor,
  headerBackground: () => (
    <View style={styles.logoContainer}>
      <TouchableOpacity
        onPress={() => {
          if (checkRouteOrigin() === 'Home') {
            navigate(RouteKey.HomeNavigator)
            navigate(RouteKey.Home)
          }
          if (checkRouteOrigin() === 'FrontpageNavigator') {
            navigate(RouteKey.FrontpageNavigator)
            navigate(RouteKey.StartupScreen)
          }
        }}>
        <Image style={styles.logo} source={Images.logo} />
      </TouchableOpacity>
    </View>
  ),
  headerTitle: () => null,
}

export const DrawerCtnOptions = {
  activeTintColor: Colors.drawer.title,
  activeBackgroundColor: null,
  inactiveTintColor: Colors.drawer.title,
  labelStyle: {
    fontFamily: Fonts.openSans,
    fontSize: 16,
  },
}

const styles = StyleSheet.create({
  logoContainer: {
    flex: 1,
    backgroundColor: Colors.headerBackground,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  logo: {
    width: 100,
    height: 50,
    resizeMode: 'contain',
  },
})
