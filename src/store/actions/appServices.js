import {apiGetAppSettings} from '../../utilities/ApiManage'
import * as act from './actionCreator'

export function setGlobalIndicatorVisibility(visible) {
  return {
    type: act.SET_GLOBAL_INDICATOR_VISIBILITY,
    visible,
  }
}

export const getAppSettings = () => {
  return async (dispatch) => {
    const response = await apiGetAppSettings()
    if (!response.ok) {
      throw new Error(response.message)
    }
    if (response.ok) {
      const galleries = []
      let isAppSetup = true
      const homeMenus = response?.data?.home_menus
      const drawerMenus = response?.data?.side_menus.filter((sideMenu) => sideMenu.icon !== '')
      const tabMenus = response?.data?.tabs.filter((tabMenu) => tabMenu.icon !== '')

      for (const key in response?.data?.galleries) {
        galleries.push(response?.data?.galleries[key].url)
      }

      for (const key in homeMenus) {
        if (!homeMenus[key]?.state) isAppSetup = false
      }

      for (const key in drawerMenus) {
        if (!drawerMenus[key]?.state) isAppSetup = false
      }

      for (const key in tabMenus) {
        if (!tabMenus[key]?.state) isAppSetup = false
      }

      dispatch({
        type: act.GET_APPSETTINGS,
        galleries: galleries,
        homeMenus: homeMenus,
        drawerMenus: drawerMenus,
        tabMenus: tabMenus,
        isAppSetup: isAppSetup
      })
    }
  }
}

export function setAppMaintenance(payload) {
  return {
    type: act.SET_APP_MAINTENANCE,
    payload,
  }
}

export function setAppState(payload) {
  return {
    type: act.SET_APP_STATE,
    payload,
  }
}

export function setInternetState(payload) {
  return {
    type: act.SET_INTERNET_STATE,
    payload,
  }
}
