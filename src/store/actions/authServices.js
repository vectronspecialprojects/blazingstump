import AsyncStorage from '@react-native-async-storage/async-storage'
import {setAccessToken} from '../../utilities/NetworkingAuth'
import * as api from '../../utilities/ApiManage'
import {deleteTags} from '../../utilities/OneSignal'
import moment from 'moment'
import {fetchAllData, fetchProfile} from './infoServices'
import * as act from './actionCreator'

export const authenticate = (user_credentials) => {
  return {type: act.AUTHENTICATE, user_credentials}
}

export const login = (email, password) => {
  return async (dispatch) => {
    const response = await api.apiLogin(email, password)
    if (!response.ok) {
      throw new Error(response.message)
    }
    if (response.ok) {
      // const expiredData = JSON.parse(atob(response.refresh_token))
      // console.log('refresh_token', expiredData)
      const user_credentials = {
        access_token: response.access_token,
        id_token: response.id_token,
        refresh_token: response.refresh_token,
        token: response.token,
        // expiry: expiredData.exp,
      }
      setAccessToken(response.token)
      saveDataToStorage(user_credentials)
      await dispatch(fetchProfile())
      dispatch(authenticate(user_credentials))
      dispatch(fetchAllData())
    }
  }
}

export const signup = (data) => {
  return async (dispatch) => {
    const response = await api.apiSignup(data)
    if (!response.ok) {
      console.log(response)
      throw new Error(response.message)
    }
    if (response.ok) {
      const user_credentials = {
        access_token: response.access_token,
        id_token: response.id_token,
        refresh_token: response.refresh_token,
        token: response.token,
      }
      setAccessToken(response.token)
      saveDataToStorage(user_credentials)
      await dispatch(fetchProfile())
      dispatch(authenticate(user_credentials))
      dispatch(fetchAllData())
    }
  }
}

export const signupOdyssey = (data) => {
  return async (dispatch) => {
    const response = await api.apiSignupOdyssey(data)
    if (!response.ok) {
      console.log(response)
      throw new Error(response.message)
    }
    if (response.ok) {
      const user_credentials = {
        access_token: response.access_token,
        id_token: response.id_token,
        refresh_token: response.refresh_token,
        token: response.token,
      }
      setAccessToken(response.token)
      saveDataToStorage(user_credentials)
      await dispatch(fetchProfile())
      dispatch(authenticate(user_credentials))
      dispatch(fetchAllData())
    }
  }
}

export const signupIGT = (data) => {
  return async (dispatch) => {
    const response = await api.apiSignupIGT(data)
    if (!response.ok) {
      console.log(response)
      throw new Error(response.message)
    }
    if (response.ok) {
      const user_credentials = {
        access_token: response.access_token,
        id_token: response.id_token,
        refresh_token: response.refresh_token,
        token: response.token,
      }
      setAccessToken(response.token)
      saveDataToStorage(user_credentials)
      await dispatch(fetchProfile())
      dispatch(authenticate(user_credentials))
      dispatch(fetchAllData())
    }
  }
}

export const logout = () => {
  AsyncStorage.removeItem('CURRENT_USER')
  deleteTags()
  return {type: act.LOGOUT}
}

const saveDataToStorage = (user_credentials) => {
  AsyncStorage.setItem('CURRENT_USER', JSON.stringify(user_credentials))
}

export const resetPassword = (oldPassword, newPassword) => {
  return async (dispatch) => {
    const res = await api.apiChangePassword(oldPassword, newPassword)
    if (!res.ok) throw new Error(res?.message)
    const user_credentials = {
      access_token: res.access_token,
      id_token: res.id_token,
      refresh_token: res.refresh_token,
      token: res.token,
    }
    setAccessToken(res.token)
    dispatch(authenticate(user_credentials))
    saveDataToStorage(user_credentials)
  }
}

export const CHANGE_EMAIL_SUCCESS = 'CHANGE_EMAIL_SUCCESS'
function changeEmailSuccess(payload) {
  return {
    type: act.CHANGE_EMAIL_SUCCESS,
    payload,
  }
}
export function changeEmail(email, password) {
  return async (dispatch) => {
    const res = await api.apiChangeEmail(email, password)
    if (!res.ok) throw new Error(res.message)
    const user_credentials = {
      access_token: res.access_token,
      id_token: res.id_token,
      refresh_token: res.refresh_token,
      token: res.token,
    }
    setAccessToken(res.token)
    dispatch(authenticate(user_credentials))
    saveDataToStorage(user_credentials)
    dispatch(changeEmailSuccess(email))
  }
}

export const refreshToken = () => {
  return async (dispatch, getState) => {
    const userCredentials = getState().authServices.user_credentials
    const res = await api.apiRefreshToken(userCredentials.refresh_token)
    if (!res.ok) throw new Error(res?.message)
    const user_credentials = {
      access_token: res.access_token,
      id_token: res.id_token,
      refresh_token: res.refresh_token,
      token: res.token,
    }
    setAccessToken(res.token)
    dispatch(authenticate(user_credentials))
    saveDataToStorage(user_credentials)
  }
}

export const verifyIdToken = () => {
  return async (dispatch, getState) => {
    const userCredentials = getState().authServices.user_credentials
    const res = await api.apiVerifyIdToken(userCredentials.id_token)
    if (!res.ok) {
      dispatch(refreshToken())
    }
    return res?.message
  }
}

export const signupMatch = (data) => {
  return async (dispatch) => {
    const response = await api.mathAccountInfo(data)
    if (!response.ok) {
      throw new Error(response.message)
    }
    if (response.ok) {
      const user_credentials = {
        access_token: response.access_token,
        id_token: response.id_token,
        refresh_token: response.refresh_token,
        token: response.token,
      }
      setAccessToken(response.token)
      saveDataToStorage(user_credentials)
      await dispatch(fetchProfile())
      dispatch(authenticate(user_credentials))
      dispatch(fetchAllData())
    }
  }
}
