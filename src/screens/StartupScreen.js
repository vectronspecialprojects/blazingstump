import React, {useEffect} from 'react'
import {View, StyleSheet, Image, ImageBackground, Text, Platform} from 'react-native'
import {HeaderButtons, Item} from 'react-navigation-header-buttons'
import {responsiveHeight, responsiveFont} from '../Themes/Metrics'

import ButtonView from './../components/ButtonView'
import HeaderButton from './../components/HeaderButton'
import Colors from '../Themes/Colors'
import Styles from '../Themes/Styles'
import RouteKey from '../navigation/RouteKey'
import Images from '../Themes/Images'
import {useSelector} from 'react-redux'
import Alert from '../components/Alert'
import {localize} from '../locale/I18nConfig'

const StartupScreen = (props) => {
  const isAppSetup = useSelector((state) => state.app.isAppSetup)

  useEffect(() => {
    if (!isAppSetup) {
      Alert.alert(localize('startUpScreen.appNotSetup'), localize('startUpScreen.message'), [
        {text: localize('okay')},
      ])
    }
  }, [isAppSetup])

  return (
    <View style={styles.screen}>
      <ImageBackground source={Images.background} style={styles.backgroundImage}>
        <View style={styles.logoContainer}>
          <Image source={Images.logoStartupScreen} style={styles.logo} />
        </View>
        <View style={styles.buttonsContainer}>
          <ButtonView
            disabled={!isAppSetup}
            title={localize('startUpScreen.welcomeBack')}
            style={{
              backgroundColor: Colors.firstPage.firstButtonBackground,
              marginBottom: responsiveHeight(8),
            }}
            titleStyle={{color: Colors.firstPage.firstButtonText}}
            descStyle={{color: Colors.firstPage.firstButtonText}}
            desc={localize('startUpScreen.alreadyAMember')}
            onPress={() => {
              props.navigation.navigate(RouteKey.LoginScreen)
            }}
          />
          <ButtonView
            disabled={!isAppSetup}
            title={localize('startUpScreen.imNew')}
            desc={localize('startUpScreen.joinNow')}
            style={{backgroundColor: Colors.firstPage.secondButtonBackground}}
            titleStyle={{color: Colors.firstPage.secondButtonText}}
            descStyle={{color: Colors.firstPage.secondButtonText}}
            onPress={() => {
              props.navigation.navigate(RouteKey.SignupScreen)
            }}
          />
          <Text style={{color: Colors.white, fontSize: responsiveFont(12), textAlign: 'center'}}>
            {localize('startUpScreen.version')}: 5.0.0
          </Text>
        </View>
      </ImageBackground>
    </View>
  )
}

export const screenOptions = (navData) => {
  return {
    headerTransparent: true,
    headerTitle: () => null,
    headerBackground: () => null,
    headerLeft: () => (
      <HeaderButtons HeaderButtonComponent={HeaderButton}>
        <Item
          style={Styles.drawer}
          title="Menu"
          iconName={Platform.OS === 'android' ? 'md-menu' : 'ios-menu'}
          onPress={() => {
            navData.navigation.toggleDrawer()
          }}
        />
      </HeaderButtons>
    ),
  }
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.defaultBackground,
  },
  backgroundImage: {
    flex: 1,
    width: '100%',
  },
  logoContainer: {
    width: '100%',
    height: '70%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  logo: {
    width: '80%',
    resizeMode: 'contain',
  },
  buttonsContainer: {
    paddingHorizontal: responsiveHeight(20),
    marginVertical: responsiveHeight(50),
    paddingVertical: responsiveHeight(50),
  },
})

export default StartupScreen
