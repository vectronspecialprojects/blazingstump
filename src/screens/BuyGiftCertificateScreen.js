import React, {useMemo, useRef, useState, useEffect} from 'react'
import {View, StyleSheet, Text, TextInput, ScrollView, KeyboardAvoidingView} from 'react-native'
import Styles from '../Themes/Styles'
import SubHeaderBar from './../components/SubHeaderBar'
import {isIOS, responsiveFont, responsiveHeight, responsiveWidth} from '../Themes/Metrics'
import Colors from '../Themes/Colors'
import TextInputView from './../components/TextInputView'
import ButtonView from './../components/ButtonView'
import {apiConfirmOrder, createGCOrder, sendToken} from '../utilities/ApiManage'
import {useDispatch, useSelector} from 'react-redux'
import {setGlobalIndicatorVisibility} from '../store/actions/appServices'
import {getStatusBarHeight, validateEmail} from '../utilities/utils'
import PaymentPopup from './ProductComponent/components/PaymentPopup'
import {fetchProfile} from '../store/actions/infoServices'
import {createPaymentMethod, initStripe} from '../utilities/Stripe'
import Toast from '../components/Toast'
import HeaderLeftButton from '../components/HeaderLeftButton'
import {localize} from '../locale/I18nConfig'

function BuyGiftCertificateScreen({route, navigation}) {
  const {listing} = route.params || {}
  const [fullName, setFullName] = useState(__DEV__ ? 'test' : '')
  const [email, setEmail] = useState(__DEV__ ? 'test@gmail.com' : '')
  const [message, setMessage] = useState(__DEV__ ? 'test' : '')
  const [isShowPayment, setShowPayment] = useState(false)
  const dispatch = useDispatch()
  const orderToken = useRef()
  const popupPaymentRef = useRef()
  const user_credentials = useSelector((state) => state.authServices.user_credentials)

  useEffect(() => {
    initStripe()
  }, [])

  const isRedeem = useMemo(() => {
    if (!listing?.products?.length) return false
    if (!!+listing?.products[0]?.product?.point_price) return true
    return false
  }, [listing])

  const total = useMemo(() => {
    return +listing?.products?.[0]?.product?.point_price || +listing?.products?.[0]?.product?.unit_price
  }, [listing])

  async function handleSubmitData() {
    try {
      if (!validateEmail(email)) throw new Error(localize('buyGiftCertificate.invalidEmail'))
      dispatch(setGlobalIndicatorVisibility(true))
      const res = await createGCOrder(
        {
          ...listing,
          type: isRedeem ? 'point' : 'cash',
          total: JSON.stringify({
            point: isRedeem ? total : 0,
            cash: isRedeem ? 0 : total,
          }),
        },
        fullName,
        email,
        message,
      )
      if (!res.ok) throw new Error(res.message)
      orderToken.current = res.token
      if (isRedeem) {
        const resConfirm = await apiConfirmOrder(res.token, 'confirmed', '')
        if (!resConfirm.ok) throw new Error(resConfirm.message)
        dispatch(fetchProfile())
        navigation.pop()
        Toast.success(localize('buyGiftCertificate.messageSuccess'))
      } else {
        setShowPayment(true)
      }
    } catch (e) {
      Toast.info(e.message)
    } finally {
      dispatch(setGlobalIndicatorVisibility(false))
    }
  }

  const allowPress = useMemo(() => {
    return email && fullName
  }, [email, fullName])

  async function handleCancelOrder() {
    try {
      setShowPayment(false)
      await apiConfirmOrder(orderToken.current, 'cancelled', '')
    } catch (e) {}
  }

  async function checkCardWithStripe(card) {
    try {
      const {name, number, cvc, expiryDate} = card
      let date = expiryDate.split('/')
      const res = await createPaymentMethod(name, number, cvc, +date[0], +date[1])
      return res.tokenId
    } catch (e) {
      popupPaymentRef.current?.setError(e.message)
      return null
    }
  }

  async function handleSubmitOrder(card) {
    try {
      let token_stripe = card.token
      if (!card.use_saved_card) {
        token_stripe = await checkCardWithStripe(card)
      }
      if (token_stripe) {
        setShowPayment(false)
        dispatch(setGlobalIndicatorVisibility(true))
        const confirmRes = await apiConfirmOrder(orderToken.current, 'confirmed', '')
        const resToken = await sendToken(orderToken.current, token_stripe, card, user_credentials)
        if (!resToken.ok) throw new Error(resToken.message)
        navigation.pop()
        Toast.success(localize('buyGiftCertificate.submitSuccess'))
      }
    } catch (e) {
      Toast.info(e.message)
    } finally {
      dispatch(setGlobalIndicatorVisibility(false))
    }
  }

  return (
    <View style={Styles.screen}>
      <KeyboardAvoidingView
        style={{flex: 1}}
        behavior={isIOS() ? 'padding' : 'height'}
        keyboardVerticalOffset={getStatusBarHeight()}>
        <SubHeaderBar title={localize('buyGiftCertificate.title')} />
        <ScrollView
          keyboardShouldPersistTaps={'handled'}
          showsVerticalScrollIndicator={false}
          bounces={false}>
          <View style={styles.container}>
            <Text style={styles.content}>
              Please enter the details of the person who will receive the gifs certificate.
            </Text>
            <TextInputView
              placeholder={localize('buyGiftCertificate.fullName')}
              onChangeText={(text) => setFullName(text)}
              value={fullName}
            />
            <TextInputView
              placeholder={localize('buyGiftCertificate.email')}
              keyboardType="email-address"
              onChangeText={(text) => setEmail(text)}
              value={email}
            />
            <TextInputView
              placeholder={localize('buyGiftCertificate.message')}
              onChangeText={(text) => setMessage(text)}
              value={message}
              inputStyle={{
                minHeight: responsiveHeight(200),
                height: null,
                alignItems: 'flex-start',
              }}
              textInputStyle={{minHeight: responsiveHeight(200)}}
              multiline={true}
              textAlignVertical={'top'}
            />
          </View>
          <ButtonView
            title={localize('buyGiftCertificate.continue')}
            disabled={!allowPress}
            style={{
              marginHorizontal: responsiveWidth(20),
              marginBottom: responsiveHeight(25),
              marginTop: responsiveHeight(100),
            }}
            onPress={() => {
              handleSubmitData()
            }}
          />
        </ScrollView>
      </KeyboardAvoidingView>
      <PaymentPopup
        visible={isShowPayment}
        onClose={() => handleCancelOrder()}
        onSubmit={(card) => {
          handleSubmitOrder(card)
        }}
        amount={total}
        ref={popupPaymentRef}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: responsiveWidth(20),
  },
  content: {
    fontSize: responsiveFont(12),
    color: Colors.white,
    textAlign: 'center',
    marginBottom: responsiveHeight(40),
    marginTop: responsiveHeight(10),
  },
})
export const screenOptions = (navData) => {
  return {
    headerLeft: () => <HeaderLeftButton navData={navData} />,
  }
}

export default BuyGiftCertificateScreen
