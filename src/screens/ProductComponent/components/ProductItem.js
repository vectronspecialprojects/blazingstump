import React, {useMemo, useState} from 'react'
import {View, StyleSheet, Text} from 'react-native'
import Colors from '../../../Themes/Colors'
import QuantityComponent from './QuantityComponent'
import {responsiveFont, responsiveHeight, responsiveWidth} from '../../../Themes/Metrics'
import Fonts from '../../../Themes/Fonts'

function ProductItem({data, onChangeQuantity, isRedeem}) {
  const {product} = data

  const [quantity, setQuantity] = useState(data.qty || 0)

  const total = useMemo(() => {
    return +product?.unit_price * quantity || 0
  }, [quantity])

  return (
    <View style={styles.container}>
      <View style={{flex: 2}}>
        <Text style={styles.name}>{product?.name}</Text>
        <Text style={styles.price}>
          {isRedeem ? 'P' : '$'}
          {product?.unit_price}
        </Text>
      </View>
      <QuantityComponent
        setValue={(value) => {
          onChangeQuantity(value)
          setQuantity(value)
        }}
        value={quantity}
      />
      <Text style={[styles.name, {flex: 1, textAlign: 'right'}]}>
        {isRedeem ? 'P' : '$'}
        {total.toFixed(isRedeem ? 0 : 2)}
      </Text>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    backgroundColor: Colors.first,
    padding: responsiveWidth(14),
    marginTop: responsiveHeight(16),
    marginHorizontal: responsiveWidth(10),
    borderRadius: responsiveWidth(10),
    alignItems: 'flex-end',
  },
  name: {
    fontSize: responsiveFont(14),
    color: Colors.white,
    fontFamily: Fonts.openSansBold,
  },
  price: {
    fontSize: responsiveFont(14),
    color: Colors.white,
  },
})

export default ProductItem
