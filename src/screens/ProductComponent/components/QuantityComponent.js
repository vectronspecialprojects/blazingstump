import React, {useEffect, useState} from 'react'
import {View, StyleSheet, TouchableOpacity, TextInput, Text} from 'react-native'
import Colors from '../../../Themes/Colors'
import {responsiveFont, responsiveHeight, responsiveWidth} from '../../../Themes/Metrics'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'

function QuantityComponent({value, setValue, maxValue = 999, style, buttonStyle, isUpdateNow, minValue = 0}) {
  const [inputValue, setInputValue] = useState(value)
  useEffect(() => {
    setInputValue(value)
  }, [value])
  return (
    <View style={[styles.container, style]}>
      <TouchableOpacity
        style={[styles.buttonDecrease, buttonStyle]}
        onPress={() => {
          if (value > minValue) setValue(value - 1)
        }}>
        <MaterialIcons name={'remove'} color={Colors.white} size={25} />
      </TouchableOpacity>
      <TextInput
        numberOfLines={1}
        maxLength={3}
        style={styles.textInput}
        placeholder={'0'}
        placeholderTextColor={Colors.white}
        onChangeText={(text) => {
          if (+text <= maxValue) {
            if (isUpdateNow) {
              setValue(+text)
            } else {
              setInputValue(text)
            }
          }
        }}
        onBlur={() => {
          setValue(+inputValue)
        }}
        value={inputValue ? `${inputValue}` : ''}
      />
      <TouchableOpacity
        style={[styles.buttonIncrease, buttonStyle]}
        onPress={() => {
          if (value < maxValue) setValue(value + 1)
        }}>
        <MaterialIcons name={'add'} color={Colors.white} size={25} />
      </TouchableOpacity>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.defaultBackground,
    flexDirection: 'row',
    height: responsiveHeight(36),
    alignItems: 'center',
  },
  buttonIncrease: {
    borderColor: Colors.white,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: responsiveWidth(5),
    backgroundColor: Colors.second,
    height: '100%',
    borderRadius: responsiveWidth(4),
  },
  buttonDecrease: {
    height: '100%',
    borderColor: Colors.white,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: responsiveWidth(5),
    backgroundColor: Colors.second,
    borderRadius: responsiveWidth(4),
  },
  textInput: {
    fontSize: responsiveFont(17),
    textAlign: 'center',
    textAlignVertical: 'center',
    marginBottom: 0,
    paddingBottom: 5,
    color: Colors.white,
    minWidth: responsiveWidth(56),
  },
})

export default QuantityComponent
