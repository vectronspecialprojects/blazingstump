import React, {useState, useEffect, useCallback, useMemo} from 'react'
import {View, Text, StyleSheet, FlatList, RefreshControl} from 'react-native'
import {useDispatch, useSelector} from 'react-redux'
import {responsiveHeight} from '../Themes/Metrics'
import {setGlobalIndicatorVisibility} from '../store/actions/appServices'
import * as infoServicesActions from '../store/actions/infoServices'
import SubHeaderBar from './../components/SubHeaderBar'
import Styles from '../Themes/Styles'
import Colors from '../Themes/Colors'
import TabsBar from './../components/TabsBar'
import CardItem from './../components/CardItem'
import VoucherTile from './../components/VoucherTile'
import Filter from './../components/Filter'
import RouteKey from '../navigation/RouteKey'
import {Voucher, Card} from '../modals/modals'
import HeaderLeftButton from '../components/HeaderLeftButton'
import Alert from '../components/Alert'
import {localize} from '../locale/I18nConfig';

const StampcardScreen = (props) => {
  const pages = props.route.params.params?.pages
  const title = props.route.params.params?.page_name
  const dispatch = useDispatch()
  const [isRefreshing, setIsRefreshing] = useState(false)
  const [selectedPage, setSelectedPage] = useState('7')
  const [isFilter, setIsFilter] = useState(false)
  const [selectedId, setSelectedId] = useState(useSelector((state) => state.infoServices.preferredVenueId))
  const stampCards = useSelector((state) => state.infoServices.stampCards)
  const stampCardsWon = useSelector((state) => state.infoServices.stampCardsWon)
  const profile = useSelector((state) => state.infoServices.profile)

  const loadContent = useCallback(async () => {
    try {
      await dispatch(infoServicesActions.fetctListings(7))
    } catch (err) {
      Alert.alert(localize('somethingWentWrong'), err.message, [{text: localize('okay')}])
    }
    try {
      await dispatch(infoServicesActions.fetctListings(8))
    } catch (err) {
      Alert.alert(localize('somethingWentWrong'), err.message, [{text: localize('okay')}])
    } finally {
      setIsRefreshing(false)
      dispatch(setGlobalIndicatorVisibility(false))
    }
  }, [dispatch])

  useEffect(() => {
    dispatch(setGlobalIndicatorVisibility(true))
    loadContent()
  }, [dispatch, loadContent])

  const tabsHandler = (id) => {
    if (selectedPage !== id) {
      setSelectedPage(id)
    }
  }

  const filterHandler = (id) => {
    setSelectedId(id)
    setIsFilter(false)
  }

  const showData = useMemo(() => {
    if (stampCards !== undefined && stampCardsWon !== undefined) {
      if (selectedPage === '8') {
        if (selectedId === 0) return stampCardsWon?.data
        return stampCardsWon?.data?.filter(
          (data) => data?.order_detail?.venue?.id === 0 || data?.order_detail?.venue?.id === selectedId,
        )
      }
      if (selectedId === 0)
        return stampCards?.data.sort((a, b) => {
          return a?.listing?.display_order - b?.listing?.display_order
        })
      return stampCards?.data
        .filter((data) => data?.listing?.venue?.id === 0 || data?.listing?.venue?.id === selectedId)
        .sort((a, b) => {
          return a?.listing?.display_order - b?.listing?.display_order
        })
    }
  }, [stampCards, stampCardsWon, selectedPage, selectedId])

  const renderItem = (itemData) => {
    const listing = itemData?.item?.listing
    const cardDetail = new Card(
      listing?.heading,
      listing?.desc_short,
      0,
      listing?.prize_promotion?.needed - listing?.prize_promotion?.accumulate,
      listing?.prize_promotion?.accumulate,
      listing?.image_banner,
    )
    return (
      <CardItem titleStyle={{color: Colors.stampcard.cardTitle}} touchDisabled={true} style={{marginBottom: responsiveHeight(5)}} cardDetail={cardDetail} />
    )
  }

  const renderGridItem = (itemData) => {
    const voucherDetail = new Voucher(
      itemData?.item?.claim_promotion?.product?.name,
      itemData?.item?.claim_promotion?.product?.desc_shot,
      itemData?.item?.barcode,
      itemData?.item?.issue_date,
      0,
      itemData?.item?.expire_date,
      itemData?.item?.voucher_type,
      itemData?.item?.amount_left,
      itemData?.item?.img,
      profile?.member?.bepoz_account_card_number,
      profile?.member?.bepoz_account_number,
    )
    return (
      <VoucherTile
        imgSource={voucherDetail?.image}
        onPress={() => {
          props.navigation.navigate(RouteKey.VoucherDetailScreen, {voucherDetail})
        }}
        title={voucherDetail?.name}
        isInProgress={!voucherDetail?.barcode}
        expiredDate={voucherDetail?.expireDate}
      />
    )
  }

  return (
    <View style={Styles.screen}>
      <SubHeaderBar
        title={title}
        filterBtn={true}
        onFilterPress={() => {
          setIsFilter(true)
        }}
      />
      <Filter
        isVisible={isFilter}
        backScreenOnPress={() => {
          setIsFilter(false)
        }}
        selectedId={selectedId}
        onFilterPress={filterHandler}
      />
      <TabsBar isShadow={true} pages={pages} onPress={tabsHandler} selectedPageId={selectedPage} />
      {selectedPage === '7' && (
        <FlatList
          data={showData}
          renderItem={renderItem}
          keyExtractor={(item) => item?.id.toString()}
          ListEmptyComponent={
            <Text style={Styles.flatlistNoItems}>No stampcards found, please check again later.</Text>
          }
          refreshControl={
            <RefreshControl
              refreshing={isRefreshing}
              onRefresh={loadContent}
              tintColor={Colors.defaultRefreshSpinner}
              titleColor={Colors.defaultRefreshSpinner}
              title={localize('pullToRefresh')}
            />
          }
        />
      )}
      {selectedPage === '8' && (
        <FlatList
          numColumns={2}
          columnWrapperStyle={{
            flex: 1,
            justifyContent: 'space-evenly',
          }}
          data={showData}
          renderItem={renderGridItem}
          keyExtractor={(item) => item?.id.toString()}
          ListEmptyComponent={
            <Text style={Styles.flatlistNoItems}>No rewards found, please check again later.</Text>
          }
          refreshControl={
            <RefreshControl
              refreshing={isRefreshing}
              onRefresh={() => {
                setIsRefreshing(true)
                loadContent()
              }}
              tintColor={Colors.defaultRefreshSpinner}
              titleColor={Colors.defaultRefreshSpinner}
              title={localize('pullToRefresh')}
            />
          }
        />
      )}
    </View>
  )
}

// export const screenOptions = (navData) => {
//   // console.log(navData.navigation);
//   return {
//     headerLeft: () => (
//       <HeaderButtons style={Styles.drawer} HeaderButtonComponent={HeaderButton}>
//         <Item
//           title="Menu"
//           iconName={
//             Platform.OS === "android" ? "md-arrow-back" : "ios-arrow-back"
//           }
//           onPress={() => {
//             navData.navigation.goBack();
//             // navData.navigation.toggleDrawer();
//           }}
//         />
//       </HeaderButtons>
//     ),
//   };
// };

export const screenOptions = (navData) => {
  return {
    headerLeft: () => <HeaderLeftButton navData={navData} />,
  }
}

const styles = StyleSheet.create({})

export default StampcardScreen
