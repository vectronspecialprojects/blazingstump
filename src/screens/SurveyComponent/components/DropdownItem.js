import React, {useState} from 'react'
import {View, StyleSheet, Text} from 'react-native'
import Colors from '../../../Themes/Colors'
import {responsiveFont, responsiveHeight, responsiveWidth} from '../../../Themes/Metrics'
import Fonts from '../../../Themes/Fonts'
import {TouchableCmp} from '../../../components/UtilityFunctions'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import DropDownList from '../../../components/DropDownList'

function DropdownItem({data, onSelect}) {
  const [showList, setShowList] = useState(false)
  const [answer, setAnswer] = useState('')
  return (
    <View style={styles.container}>
      <View style={styles.titleWrapper}>
        <Text style={styles.title}>{data?.question}</Text>
      </View>
      <TouchableCmp onPress={() => setShowList(true)}>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <Text style={styles.answer}>{answer ? answer.answer : 'Please select one answer'}</Text>
          <MaterialIcons name="keyboard-arrow-down" size={24} color={Colors.white} />
        </View>
      </TouchableCmp>
      <DropDownList
        data={data.answers}
        visible={showList}
        modalTitle={'Please select one answer'}
        height={responsiveHeight(300)}
        showValue={'answer'}
        onClose={() => setShowList(false)}
        value={answer.answer}
        onSelect={(item) => {
          onSelect(item.id)
          setAnswer(item)
        }}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.survey.background,
    marginHorizontal: responsiveWidth(10),
    marginTop: responsiveHeight(20),
    borderRadius: responsiveHeight(10),
    paddingHorizontal: responsiveWidth(16),
  },
  title: {
    fontSize: responsiveFont(14),
    fontFamily: Fonts.openSansBold,
    color: Colors.survey.questionTitle,
  },
  titleWrapper: {
    borderBottomWidth: 0.5,
    borderColor: Colors.survey.background,
    paddingVertical: responsiveHeight(16),
  },
  answer: {
    fontSize: responsiveFont(13),
    fontFamily: Fonts.openSans,
    color: Colors.survey.description,
    marginVertical: responsiveHeight(10),
    flex: 1,
  },
})

export default DropdownItem
