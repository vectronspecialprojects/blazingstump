import React, {useState} from 'react'
import {View, StyleSheet, Text} from 'react-native'
import {responsiveFont, responsiveHeight, responsiveWidth} from '../../../Themes/Metrics'
import Fonts from '../../../Themes/Fonts'
import Colors from '../../../Themes/Colors'
import RadioButton from '../../../components/RadioButton';

function MultipleChoiceItem({onSelect, data}) {
  const [answerId, setAnswerId] = useState(data.answer_id)
  return (
    <View style={styles.container}>
      <View style={styles.titleWrapper}>
        <Text style={styles.title}>{data?.question}</Text>
      </View>
      {data?.answers?.map((item, index) => {
        return (
          <RadioButton
            style={{paddingVertical: responsiveHeight(5)}}
            title={item.answer}
            onPress={() => {
              setAnswerId(item.id)
              onSelect(item.id)
            }}
            value={answerId === item.id}
          />
        )
      })}
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.survey.background,
    marginHorizontal: responsiveWidth(10),
    marginTop: responsiveHeight(20),
    borderRadius: responsiveHeight(10),
    padding: responsiveWidth(16),
  },
  title: {
    fontSize: responsiveFont(14),
    fontFamily: Fonts.openSansBold,
    color: Colors.survey.questionTitle,
  },
  titleWrapper: {
    borderBottomWidth: 0.5,
    borderColor: Colors.survey.background,
    paddingBottom: responsiveHeight(16),
  },
})

export default MultipleChoiceItem
