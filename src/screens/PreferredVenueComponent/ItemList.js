import React from 'react'
import {View, Text, StyleSheet} from 'react-native'
import Styles from '../../Themes/Styles'
import Colors from '../../Themes/Colors'
import {TouchableCmp} from '../../components/UtilityFunctions'
import {responsiveFont, responsiveHeight, responsiveWidth} from '../../Themes/Metrics'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'

const ItemList = (props) => {
  return (
    <TouchableCmp onPress={props.onPress}>
      <View style={[styles.container, props.style]}>
        <Text style={[Styles.mediumCapText, props.textStyle]}>{props.data.name}</Text>
        {props.isArrowShowed &&
          (props.selectedItem === props.data.id && props.isOpen ? (
            <FontAwesome5 name="chevron-down" size={responsiveFont(10)} color={Colors.preferredVenue.icon} />
          ) : (
            <FontAwesome5 name="chevron-right" size={responsiveFont(10)} color={Colors.preferredVenue.icon} />
          ))}
      </View>
    </TouchableCmp>
  )
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: responsiveWidth(20),
    marginTop: responsiveHeight(25),
  },
})

export default ItemList
