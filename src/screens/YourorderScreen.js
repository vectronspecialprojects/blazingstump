import React, {useMemo, useEffect} from 'react'
import {View, ActivityIndicator} from 'react-native'
import {WebView} from 'react-native-webview'
import {useSelector} from 'react-redux'
import Colors from '../Themes/Colors'
import Styles from '../Themes/Styles'
import HeaderLeftButton from '../components/HeaderLeftButton'
import Alert from '../components/Alert'
import {venueWebsite} from '../constants/env'
import {localize} from '../locale/I18nConfig'

const YouorderScreen = (props) => {
  const dynamicUri = useSelector(
    (state) => state.infoServices.profile.member.current_preferred_venue_full?.your_order_link,
  )
  const idToken = useSelector((state) => state.authServices.user_credentials?.id_token)
  const staticUri = props.route.params.params?.special_link
  const qrcodeUri = props.route.params.params?.qrcodeUri

  const uri = useMemo(() => {
    if (qrcodeUri) return qrcodeUri
    if (staticUri) return staticUri
    else if (!staticUri && dynamicUri) return dynamicUri
    return venueWebsite
  }, [dynamicUri, staticUri, qrcodeUri])

  useEffect(() => {
    if (!uri) {
      Alert.alert(localize('yourOrder.notAvailable'), localize('yourOrder.message'), [
        {text: localize('okay')},
      ])
    }
  }, [uri])

  return (
    <View style={Styles.screen}>
      <View style={{flex: 1}}>
        <WebView
          startInLoadingState={true}
          source={{uri: `${uri}&id_token=${idToken}`}}
          renderLoading={() => (
            <View
              style={{
                backgroundColor: Colors.defaultBackground,
                position: 'absolute',
                top: '50%',
                left: '50%',
                right: '50%',
              }}>
              <ActivityIndicator size="large" />
            </View>
          )}
        />
      </View>
    </View>
  )
}

export const screenOptions = (navData) => {
  return {
    headerLeft: () => <HeaderLeftButton navData={navData} />,
  }
}

export default YouorderScreen
