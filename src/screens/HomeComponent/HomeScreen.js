import React, {useState, useEffect, useCallback} from 'react'
import {
  View,
  ScrollView,
  Button,
  StyleSheet,
  Text,
  RefreshControl,
  Image,
  ImageBackground,
} from 'react-native'
import {useDispatch, useSelector} from 'react-redux'
import {setGlobalIndicatorVisibility} from '../../store/actions/appServices'
import * as infoServicesActions from '../../store/actions/infoServices'
import PreferredVenueButton from './../../components/PreferredVenueButton'
import Swiper from 'react-native-swiper'
import MemberNameBar from './../../components/MemberNameBar'
import Colors from '../../Themes/Colors'
import Styles from '../../Themes/Styles'
import HomeTilesBar from './../../components/HomeTilesBar'
import BarcodeBar from './../../components/BarcodeBar'
import StarButton from './../../components/StarButton'
import RouteKey from '../../navigation/RouteKey'
import {deviceWidth, isIOS, responsiveHeight, responsiveWidth} from '../../Themes/Metrics'
import Notification from './../../components/Notification'
import FastImage from 'react-native-fast-image'
import Images from '../../Themes/Images'
import HeaderLeftButton from '../../components/HeaderLeftButton'
import {
  showTier,
  isShowStarBar,
  isShowPreferredVenue,
  showTierBelowName,
  showPointsBelowName,
  isGaming,
} from '../../constants/env'
import Alert from '../../components/Alert'
import {isEmptyValues} from '../../utilities/utils'
import {apiIsBepozAccountCreated} from '../../utilities/ApiManage'
import {localize} from '../../locale/I18nConfig'

const HomeScreen = (props) => {
  const galleries = useSelector((state) => state.app.galleries)
  const profile = useSelector((state) => state.infoServices.profile)
  const notifications = useSelector((state) => state.infoServices.notifications)
  const homeMenus = useSelector((state) => state.app.homeMenus)
  const dispatch = useDispatch()
  const [isRefreshing, setIsRefreshing] = useState(false)
  const internetState = useSelector((state) => state.app.internetState)
  const [didMount, setDidMount] = useState(false)

  const ifRoute = (id) => {
    const homeMenu = homeMenus.filter((menu) => {
      return menu.id === id
    })
    if (
      homeMenu[0].icon === '' ||
      homeMenu[0].state === '' ||
      homeMenu[0].state === 'none' ||
      (homeMenu[0].page_layout === 'special_view' &&
        (homeMenu[0].special_page === 'points' ||
          homeMenu[0].special_page === 'redeems' ||
          homeMenu[0].special_page === 'balance' ||
          homeMenu[0].special_page === 'saved'))
    ) {
      return null
    } else {
      props.navigation.navigate('homeMenu' + id)
    }
  }

  const loadContent = useCallback(async () => {
    try {
      await dispatch(infoServicesActions.fetchProfile())
    } catch (err) {
      Alert.alert(localize('somethingWentWrong'), err.message, [{text: localize('okay')}])
    }
    try {
      await dispatch(infoServicesActions.getNotification())
    } catch (err) {
      Alert.alert(localize('somethingWentWrong'), err.message, [{text: localize('okay')}])
    } finally {
      setIsRefreshing(false)
      setGlobalIndicatorVisibility(false)
    }
  }, [])

  const checkBepozAccount = useCallback(async () => {
    const response = await apiIsBepozAccountCreated(profile?.email)
    if (!response.ok && profile.email_confirmation) {
      Alert.alert(localize('canNotMatch'), response.message, [{text: localize('okay')}])
    }
  }, [profile])

  useEffect(() => {
    setDidMount(true)
  }, [])

  useEffect(() => {
    if (didMount && !isEmptyValues(profile) && !profile?.member?.bepoz_account_card_number) {
      if (isGaming) checkBepozAccount()
      if (!profile.email_confirmation) {
        Alert.alert(localize('home.titleConfirmEmail'), localize('home.messageConfirmEmail'), [
          {text: localize('okay')},
        ])
      }
    }
  }, [profile])

  useEffect(() => {
    const unsubscribe = props.navigation.addListener('focus', () => {
      if (internetState) {
        setGlobalIndicatorVisibility(true)
        loadContent()
      }
    })
    return () => {
      unsubscribe()
    }
  }, [])

  return (
    <View style={Styles.screen}>
      <ScrollView
        refreshControl={
          <RefreshControl
            refreshing={isRefreshing}
            onRefresh={() => {
              if (internetState) {
                setIsRefreshing(true)
                loadContent()
              }
            }}
            tintColor={Colors.defaultRefreshSpinner}
            titleColor={Colors.defaultRefreshSpinner}
            title={localize('pullToRefresh')}
          />
        }>
        <View style={{...Styles.centerContainer, ...{height: responsiveHeight(300)}}}>
          <Swiper loop={true} style={{height: responsiveHeight(300)}} autoplay={true} showsPagination={false}>
            {galleries.map((item, index) => {
              return (
                <FastImage
                  key={index.toString()}
                  source={{uri: item}}
                  style={{width: deviceWidth(), height: responsiveHeight(300)}}
                />
              )
            })}
          </Swiper>
        </View>

        {isShowStarBar && (
          <View>
            <View
              style={{
                ...styles.tiersContainer,
                ...{
                  backgroundColor: profile?.member?.member_tier?.tier?.color || Colors.second,
                },
              }}
            />
          </View>
        )}

        <View style={{backgroundColor: Colors.defaultBackground}}>
          {isShowStarBar && (
            <StarButton
              style={{marginBottom: responsiveHeight(10)}}
              color={profile?.member?.member_tier?.tier?.color || Colors.second}
            />
          )}
          {notifications?.length >= 1 && (
            <Notification notifications={notifications} navigation={props.navigation} />
          )}
          <MemberNameBar
            name={profile}
            onIconPress={() => {
              props.navigation.navigate(RouteKey.ProfileScreen)
            }}
            style={{marginTop: isShowStarBar ? 0 : responsiveHeight(24)}}
          />
          {showTierBelowName && (
            <Text style={Styles.smallCapText}>{profile?.member?.member_tier?.tier?.name}</Text>
          )}
          {showPointsBelowName && (
            <Text style={[Styles.smallNormalText, {alignSelf: 'center'}]}>
              POINTS BALANCE: {+profile?.member?.numberOf?.points}
            </Text>
          )}
          <HomeTilesBar
            homeMenus={homeMenus}
            userInfo={profile}
            onPress={ifRoute}
            marginVertical={responsiveHeight(30)}
          />
        </View>

        {!isShowPreferredVenue && (
          <View
            style={{
              ...styles.singleTierBar,
              ...{backgroundColor: profile?.member?.member_tier?.tier?.color || Colors.home.tierBackground},
            }}>
            {showTier && (
              <Text style={{...Styles.largeCapText, ...{color: Colors.home.tierBarText}}}>
                {profile?.member?.member_tier?.tier?.name}
              </Text>
            )}
          </View>
        )}

        {isShowPreferredVenue && (
          <PreferredVenueButton
            style={{
              marginBottom: responsiveHeight(30),
              backgroundColor:
                profile?.member?.current_preferred_venue_full?.is_preffered_venue_color === 'true'
                  ? profile?.member?.current_preferred_venue_full?.color
                  : Colors.home.preferredVenueButton,
            }}
            icon="up"
            venueName={profile?.member?.current_preferred_venue_name}
            ifOnlineOrdering={profile?.member?.current_preferred_venue_full?.your_order_integration}
            onPress={() => {
              props.navigation.navigate(RouteKey.PreferredVenueScreen)
            }}
          />
        )}

        {!!profile?.member?.bepoz_account_card_number ? (
          <BarcodeBar
            value={profile?.member?.bepoz_account_card_number}
            width={responsiveWidth(1.25)}
            title={localize('memberNumber')}
            text={profile?.member?.bepoz_account_number}
          />
        ) : (
          <ImageBackground style={styles.barcode} source={Images.barcode}>
            <View style={styles.textWrapper}>
              <Text style={[Styles.xSmallNormalText, {color: Colors.home.barcodeGeneratingText}]}>
                Your membership barcode is being generated.
              </Text>
            </View>
          </ImageBackground>
        )}
      </ScrollView>
    </View>
  )
}

export const screenOptions = (navData) => {
  return {
    headerTransparent: true,
    headerTitle: () => null,
    headerLeft: () => (
      <HeaderLeftButton
        navData={navData}
        iconName={!isIOS() ? 'md-menu' : 'ios-menu'}
        onPress={() => {
          navData.navigation.toggleDrawer()
        }}
      />
    ),
    headerBackground: () => (
      <View style={styles.logoContainer}>
        <Image style={styles.logo} source={Images.logo} />
      </View>
    ),
  }
}

const styles = StyleSheet.create({
  logoContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    paddingRight: responsiveWidth(20),
  },
  logo: {
    width: responsiveWidth(100),
    height: responsiveHeight(50),
    resizeMode: 'contain',
  },
  tiersContainer: {
    width: '100%',
    height: responsiveHeight(37),
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#65c8c6',
  },
  barcode: {
    width: responsiveWidth(200),
    height: responsiveHeight(50),
    alignSelf: 'center',
  },
  textWrapper: {
    flex: 1,
    paddingHorizontal: responsiveWidth(2),
    backgroundColor: 'rgba(0, 0, 0, 0.6)',
    justifyContent: 'center',
    alignItems: 'center',
  },
  singleTierBar: {
    height: responsiveHeight(55),
    marginBottom: responsiveHeight(30),
    justifyContent: 'center',
  },
})

export default HomeScreen
