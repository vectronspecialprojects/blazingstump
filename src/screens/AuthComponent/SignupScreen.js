import React, {useReducer, useCallback, useState, useMemo, useEffect} from 'react'
import {View, StyleSheet, KeyboardAvoidingView, ScrollView} from 'react-native'
import {useDispatch, useSelector} from 'react-redux'
import {isIOS, responsiveHeight, responsiveWidth} from '../../Themes/Metrics'
import {setGlobalIndicatorVisibility} from '../../store/actions/appServices'
import * as authServicesActions from '../../store/actions/authServices'
import Colors from '../../Themes/Colors'
import Styles from '../../Themes/Styles'
import Fonts from '../../Themes/Fonts'
import SubHeaderBar from '../../components/SubHeaderBar'
import Dropdown from '../../components/Dropdown'
import Input from '../../components/Input'
import DateInput from '../../components/DateInput'
import ButtonView from '../../components/ButtonView'
import {signupData} from '../../modals/modals'
import TermAndCondition from '../../components/TermAndCondition'
import {getStatusBarHeight, isEmptyValues} from '../../utilities/utils'
import HeaderLeftButton from '../../components/HeaderLeftButton'
import {
  isMultiVenue,
  showTierSignup,
  isGaming,
  gamingOdyssey,
  gamingIGT,
  isCustomField,
  showMultipleExtended,
} from '../../constants/env'
import * as api from '../../utilities/ApiManage'
import Alert from '../../components/Alert'
import {localize} from '../../locale/I18nConfig'

const FORM_INPUT_UPDATE = 'FORM_INPUT_UPDATE'
const formReducer = (state, action) => {
  if (action.type === FORM_INPUT_UPDATE) {
    const updatedValues = {
      ...state.inputValues,
      [action.input]: action.value,
    }
    const updatedValidities = {
      ...state.inputValidities,
      [action.input]: action.isValid,
    }
    let updatedFormIsValid = true
    for (const key in updatedValidities) {
      updatedFormIsValid = updatedFormIsValid && updatedValidities[key]
    }
    return {
      formIsValid: updatedFormIsValid,
      inputValidities: updatedValidities,
      inputValues: updatedValues,
    }
  }
  return state
}

const SignupScreen = (props) => {
  const dispatch = useDispatch()
  const venues = useSelector((state) => state.infoServices.venues)
  const [listTier, setListTier] = useState('')
  const [options, setOptions] = useState([])
  const [gamingFields, setGamingFields] = useState([])

  const [formState, dispatchFormState] = useReducer(formReducer, {
    inputValues: {
      venue: '',
      first_name: '',
      last_name: '',
      email: '',
      password: '',
      password_confirmation: '',
      mobile: '',
      dob: '',
      tier: '',
      custom_field_1: '',
      custom_field_2: '',
    },
    inputValidities: {
      venue: !isMultiVenue,
      first_name: false,
      last_name: false,
      email: false,
      password: false,
      password_confirmation: false,
      mobile: false,
      dob: false,
      tier: !showTierSignup,
    },
    formIsValid: false,
  })

  useEffect(() => {
    if (showTierSignup || isCustomField) getTier()
    if (isGaming) getGaming()
  }, [])

  useEffect(() => {
    if (isCustomField && !showMultipleExtended) inputChangeHandler('custom_field_2', '', false)
  }, [formState.inputValues?.custom_field_1])

  async function getTier() {
    try {
      const response = await api.apiGetTiers()
      if (!response.ok) {
        throw new Error(response?.message)
      }
      setListTier(response.data)
      if (isCustomField) {
        let list
        if (showMultipleExtended) {
          list = response?.bepozcustomfield?.extended_value
          list?.map((e) => {
            if (e?.fieldType === 'Text') {
              dispatchFormState({
                type: FORM_INPUT_UPDATE,
                input: e?.id,
                value: '',
                isValid: false,
              })
            }
          })
        } else {
          list = formatValue(response?.bepozcustomfield?.extended_value)
        }
        setOptions(list)
      }
    } catch (e) {
      Alert.alert(localize('signUp.titleGetTier'), e.message, [{text: localize('okay')}])
    }
  }

  async function getGaming() {
    try {
      const response = await api.apiGetGaming()
      console.log('gaming', response)
      if (!response.ok) {
        throw new Error(response?.message)
      }
      let list = response?.gaming_mandatory_field?.extended_value
      list?.map((e) => {
        if (e?.fieldType === 'Text') {
          dispatchFormState({
            type: FORM_INPUT_UPDATE,
            input: e?.id,
            value: '',
            isValid: false,
          })
        }
      })
      setGamingFields(list)
    } catch (e) {
      Alert.alert(localize('signUp.titleGetGaming'), e.message, [{text: localize('okay')}])
    }
  }

  function formatValue(data) {
    if (!data?.length) return {}
    const obj = {}
    data?.map((item) => {
      let multiValues = []
      item.multiValues?.map((item) => {
        multiValues.push({
          label: item,
          value: item,
        })
      })
      item.multiValues = multiValues
      obj[item.id] = item
    })
    return obj
  }

  const inputChangeHandler = useCallback(
    (inputIdentifier, inputValue, inputValidity) => {
      console.log(inputIdentifier, inputValidity)
      dispatchFormState({
        type: FORM_INPUT_UPDATE,
        input: inputIdentifier,
        value: inputValue,
        isValid: inputValidity,
      })
    },
    [dispatchFormState],
  )

  const signupHandler = async () => {
    const data = new signupData(
      formState.inputValues.venue,
      formState.inputValues.first_name,
      formState.inputValues.last_name,
      formState.inputValues.email,
      formState.inputValues.dob,
      formState.inputValues.password,
      formState.inputValues.password_confirmation,
      formState.inputValues.mobile,
      true,
      formState.inputValues.tier,
    )
    dispatch(setGlobalIndicatorVisibility(true))
    if (isCustomField) {
      if (showMultipleExtended) {
        options?.map((e) => {
          data[e?.id] = formState.inputValues[e?.id]
        })
      } else {
        data[formState.inputValues?.custom_field_1] = formState.inputValues?.custom_field_2
      }
    }
    if (isGaming) {
      gamingFields?.map((e) => {
        data[e?.id] = formState.inputValues[e?.id]
      })
    }
    try {
      if (gamingOdyssey && !gamingIGT) {
        await dispatch(authServicesActions.signupOdyssey(data))
      } else if (!gamingOdyssey && gamingIGT) {
        await dispatch(authServicesActions.signupIGT(data))
      } else {
        await dispatch(authServicesActions.signup(data))
      }
    } catch (err) {
      Alert.alert(localize('signUp.titleErrSignUp'), err.message, [{text: localize('okay')}])
    } finally {
      dispatch(setGlobalIndicatorVisibility(false))
    }
  }

  const dropDownItems = useMemo(() => {
    const items = []
    for (const key in venues) {
      items.push({label: venues[key].name, value: venues[key].id})
    }
    return items
  }, [venues])

  const dropDownTier = useMemo(() => {
    if (!listTier?.length) return []
    const list = []
    listTier?.map((item) => {
      list.push({
        label: item?.name,
        value: item?.id,
      })
    })
    return list
  }, [listTier])

  const customFields = (values) => {
    if (isEmptyValues(values)) return []
    let list = []
    Object.values(values).map((item) => {
      list.push({
        label: item?.fieldName || item,
        value: item?.id || item,
      })
    })
    return list
  }

  const renderExtendValue = useCallback(() => {
    return options?.map((e) => {
      if (e?.fieldType === 'Dropdown') {
        return (
          <Dropdown
            id={e?.id}
            placeholder={e?.fieldDisplayTitle}
            required
            onInputChange={inputChangeHandler}
            label={e?.fieldDisplayTitle}
            items={customFields(e?.multiValues)}
          />
        )
      } else if (e?.fieldType === 'Text') {
        return (
          <Input
            id={e?.id}
            label={e?.fieldDisplayTitle}
            alphabet
            keyboardType="default"
            required
            minLength={2}
            autoCapitalize="words"
            placeholder={e?.fieldDisplayTitle}
            onInputChange={inputChangeHandler}
            initialValue=""
            placeholderTextColor={Colors.signUp.placeholderTextColor}
            style={{
              backgroundColor: Colors.signUp.fieldBackground,
              color: Colors.signUp.fieldText,
            }}
            errorText={`Please enter a valid ${e?.fieldName}.`}
          />
        )
      }
    })
  }, [options])

  const renderGaming = useCallback(() => {
    return gamingFields?.map((e) => {
      if (e?.fieldType === 'Dropdown') {
        return (
          <Dropdown
            id={e?.id}
            placeholder={e?.fieldDisplayTitle}
            required
            onInputChange={inputChangeHandler}
            label={e?.fieldDisplayTitle}
            items={customFields(e?.multiValues)}
          />
        )
      } else if (e?.fieldType === 'Text') {
        return (
          <Input
            id={e?.id}
            label={e?.fieldDisplayTitle}
            //alphabet
            keyboardType="default"
            required
            minLength={2}
            autoCapitalize="words"
            placeholder={e?.fieldDisplayTitle}
            onInputChange={inputChangeHandler}
            initialValue=""
            placeholderTextColor={Colors.signUp.placeholderTextColor}
            style={{
              backgroundColor: Colors.signUp.fieldBackground,
              color: Colors.signUp.fieldText,
            }}
            errorText={`${localize('signUp.plsEnter')} ${e?.fieldName}.`}
          />
        )
      }
    })
  }, [gamingFields])

  return (
    <View style={Styles.screen}>
      <SubHeaderBar title={localize('signUp.signUp')} />
      <KeyboardAvoidingView
        style={{paddingHorizontal: responsiveWidth(20)}}
        behavior={isIOS() ? 'padding' : 'height'}
        keyboardVerticalOffset={getStatusBarHeight()}>
        <ScrollView keyboardShouldPersistTaps={'handled'} showsVerticalScrollIndicator={false}>
          {isMultiVenue && (
            <Dropdown
              id="venue"
              required
              onInputChange={inputChangeHandler}
              label={localize('signUp.preferredVenue')}
              placeholder={localize('signUp.preferredVenuePlaceholder')}
              items={dropDownItems}
            />
          )}
          {showTierSignup && (
            <Dropdown
              id="tier"
              required
              onInputChange={inputChangeHandler}
              label={localize('signUp.tier')}
              placeholder={localize('signUp.tierPlaceholder')}
              items={dropDownTier}
            />
          )}
          <Input
            id="first_name"
            label={localize('signUp.firstName')}
            placeholder={localize('signUp.firstName')}
            alphabet
            keyboardType="default"
            required
            minLength={2}
            autoCapitalize="words"
            errorText={`${localize('signUp.plsEnter')} ${localize('signUp.firstName')}`}
            onInputChange={inputChangeHandler}
            initialValue=""
            placeholderTextColor={Colors.signUp.placeholderTextColor}
            style={{
              backgroundColor: Colors.signUp.fieldBackground,
              color: Colors.signUp.fieldText,
            }}
          />
          <Input
            id="last_name"
            label={localize('signUp.lastName')}
            placeholder={localize('signUp.lastName')}
            alphabet
            keyboardType="default"
            required
            minLength={2}
            autoCapitalize="words"
            errorText={`${localize('signUp.plsEnter')} ${localize('signUp.lastName')}`}
            onInputChange={inputChangeHandler}
            initialValue=""
            placeholderTextColor={Colors.signUp.placeholderTextColor}
            style={{
              backgroundColor: Colors.signUp.fieldBackground,
              color: Colors.signUp.fieldText,
            }}
          />

          {isGaming && renderGaming()}

          {!isEmptyValues(options) && isCustomField && (
            <View>
              {showMultipleExtended ? (
                renderExtendValue()
              ) : (
                <Dropdown
                  id="custom_field_1"
                  placeholder={localize('signUp.field_1Placeholder')}
                  required
                  onInputChange={inputChangeHandler}
                  label={localize('signUp.field_1')}
                  items={customFields(options)}
                />
              )}
            </View>
          )}

          {!!formState.inputValues?.custom_field_1 && isCustomField && (
            <Dropdown
              key={formState.inputValues?.custom_field_1}
              id="custom_field_2"
              placeholder={localize('signUp.field_2Placeholder')}
              required
              onInputChange={inputChangeHandler}
              label={localize('signUp.field_2')}
              items={options[formState.inputValues?.custom_field_1]?.multiValues}
            />
          )}

          <Input
            id="email"
            label={localize('signUp.email')}
            placeholder={localize('signUp.email')}
            keyboardType="email-address"
            required
            email
            emailAvailability
            autoCapitalize="none"
            errorText={`${localize('signUp.plsEnter')} ${localize('signUp.email')}`}
            onInputChange={inputChangeHandler}
            initialValue=""
            placeholderTextColor={Colors.signUp.placeholderTextColor}
            style={{
              backgroundColor: Colors.signUp.fieldBackground,
              color: Colors.signUp.fieldText,
            }}
          />
          <DateInput
            id="dob"
            required
            onInputChange={inputChangeHandler}
            label={localize('signUp.dob')}
            style={{
              backgroundColor: Colors.signUp.fieldBackground,
            }}
            errorText={localize('signUp.dobErr')}
          />
          <Input
            id="password"
            label={localize('signUp.password')}
            placeholder={localize('signUp.password')}
            keyboardType="default"
            secureTextEntry
            required
            password
            autoCapitalize="none"
            errorText={`${localize('signUp.plsEnter')} ${localize('signUp.password')}`}
            onInputChange={inputChangeHandler}
            initialValue=""
            placeholderTextColor={Colors.signUp.placeholderTextColor}
            style={{
              backgroundColor: Colors.signUp.fieldBackground,
              color: Colors.signUp.fieldText,
            }}
          />
          <Input
            id="password_confirmation"
            label={localize('signUp.confirmPassword')}
            placeholder={localize('signUp.confirmPassword')}
            keyboardType="default"
            secureTextEntry
            required
            resetEvent={formState.inputValues.password.length <= 1}
            confirmPassword
            crossCheckPassword={formState.inputValues.password}
            autoCapitalize="none"
            errorText={localize('signUp.confirmPassErr')}
            onInputChange={inputChangeHandler}
            initialValue=""
            placeholderTextColor={Colors.signUp.placeholderTextColor}
            style={{
              backgroundColor: Colors.signUp.fieldBackground,
              color: Colors.signUp.fieldText,
            }}
          />
          <Input
            id="mobile"
            label={localize('signUp.mobile')}
            placeholder={localize('signUp.mobile')}
            keyboardType="phone-pad"
            required
            minLength={10}
            maxLength={10}
            autoCapitalize="none"
            errorText={`${localize('signUp.plsEnter')} ${localize('signUp.mobile')}`}
            onInputChange={inputChangeHandler}
            initialValue=""
            placeholderTextColor={Colors.signUp.placeholderTextColor}
            style={{
              backgroundColor: Colors.signUp.fieldBackground,
              color: Colors.signUp.fieldText,
            }}
          />
          <View style={{marginTop: responsiveHeight(20)}}>
            <TermAndCondition />
          </View>
          <View style={styles.buttonContainer}>
            <ButtonView
              titleStyle={{color: Colors.signUp.textSignupButton}}
              style={{backgroundColor: Colors.signUp.backgroundSignupButton}}
              title={localize('signUp.continue')}
              onPress={signupHandler}
              disabled={!formState.formIsValid}
            />
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    </View>
  )
}

export const screenOptions = (navData) => {
  return {
    headerLeft: () => <HeaderLeftButton navData={navData} />,
  }
}

const styles = StyleSheet.create({
  term: {
    color: Colors.defaultTextColor,
    fontFamily: Fonts.openSans,
    fontSize: 12,
    textAlign: 'center',
    marginTop: responsiveHeight(15),
  },
  buttonContainer: {
    marginTop: responsiveHeight(15),
    marginBottom: responsiveHeight(70),
  },
})

export default SignupScreen
