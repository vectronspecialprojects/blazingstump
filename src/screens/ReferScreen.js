import React, {useMemo, useState, useCallback, useEffect} from 'react'
import {View, StyleSheet, Text} from 'react-native'
import * as infoServicesActions from '../store/actions/infoServices'
import SubHeaderBar from './../components/SubHeaderBar'
import Styles from '../Themes/Styles'
import {responsiveFont, responsiveHeight, responsiveWidth} from '../Themes/Metrics'
import Colors from '../Themes/Colors'
import TextInputView from './../components/TextInputView'
import ButtonView from './../components/ButtonView'
import {useDispatch, useSelector} from 'react-redux'
import {setGlobalIndicatorVisibility} from '../store/actions/appServices'
import {apiSubmitRefer} from '../utilities/ApiManage'
import Toast from '../components/Toast'
import HeaderLeftButton from '../components/HeaderLeftButton'
import Alert from '../components/Alert'
import {localize} from '../locale/I18nConfig'

const ReferScreen = ({navigation}) => {
  const [fullName, setFullName] = useState('')
  const [email, setEmail] = useState('')
  const msg = useSelector((state) => state.infoServices.referMsg)
  const dispatch = useDispatch()

  const loadContent = useCallback(async () => {
    try {
      await dispatch(infoServicesActions.fetctReferMsg())
    } catch (err) {
      Alert.alert(localize('somethingWentWrong'), err.message, [{text: localize('okay')}])
    } finally {
      dispatch(setGlobalIndicatorVisibility(false))
    }
  }, [dispatch])

  useEffect(() => {
    dispatch(setGlobalIndicatorVisibility(true))
    loadContent()
  }, [dispatch, loadContent])

  async function handleSubmit() {
    try {
      dispatch(setGlobalIndicatorVisibility(true))
      const res = await apiSubmitRefer(email, fullName)
      if (!res.ok) throw new Error(res.message)
      Toast.success(localize('refer.invitationSuccess'))
      navigation.navigate('Home')
    } catch (err) {
      Alert.alert(localize('somethingWentWrong'), err.message, [{text: localize('okay')}])
    } finally {
      dispatch(setGlobalIndicatorVisibility(false))
    }
  }

  const isAllow = useMemo(() => {
    return fullName && email
  }, [fullName, email])

  return (
    <View style={Styles.screen}>
      <SubHeaderBar title={localize('refer.title')} />
      <View style={styles.container}>
        <Text style={styles.content}>{msg}</Text>
        <TextInputView
          placeholder={localize('refer.fullName')}
          placeholderTextColor={Colors.referFriends.placeHolder}
          onChangeText={(text) => setFullName(text)}
          value={fullName}
          textInputStyle={{color: Colors.referFriends.textInput}}
          inputStyle={{backgroundColor: Colors.referFriends.textInputBackground}}
        />
        <TextInputView
          textInputStyle={{color: Colors.referFriends.textInput}}
          inputStyle={{backgroundColor: Colors.referFriends.textInputBackground}}
          placeholderTextColor={Colors.referFriends.placeHolder}
          keyboardType={'email-address'}
          placeholder={localize('refer.email')}
          onChangeText={(text) => setEmail(text)}
          value={email}
        />
        <ButtonView
          title={localize('refer.continue')}
          disabled={!isAllow}
          style={{marginTop: responsiveHeight(34)}}
          onPress={() => handleSubmit()}
        />
      </View>
    </View>
  )
}

export const screenOptions = (navData) => {
  return {
    headerLeft: () => <HeaderLeftButton navData={navData} />,
  }
}

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: responsiveWidth(20),
  },
  content: {
    fontSize: responsiveFont(12),
    color: Colors.defaultTextColor,
    textAlign: 'center',
    marginBottom: responsiveHeight(40),
    marginTop: responsiveHeight(10),
  },
})

export default ReferScreen
