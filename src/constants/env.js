import {Platform} from 'react-native'

const buildEvn = {
  production: 'production',
  staging: 'staging',
}

const vars = {
  googleApiKey: 'AIzaSyAl60Q1FOsJeZ4RGaF5KCp_Kcf9sGM6p3A',
  oneSignalApiKey: 'd3442f74-12c9-4e82-8822-d6273726036f',
  buildEvn: buildEvn.staging,
}

export const codePushKey = Platform.select({
  ios: {
    staging: 'E2KZ50Zc8ymB_0FF8y7ULIvb5SBxa4qaJ2s7N',
    production: 'Z6N0NXwUxJ-yq-Mr2Hme_-C_wWefUYoMbCy7-',
  },
  android: {
    staging: 'Op3L7bGKPvGoAnu6OmKhxewkLCbMU423Km348',
    production: '03i7596Pzj04CHnCHQZDTbJqUr10mNkAmJjTS',
  },
})

export const venueName = 'My Place'
export const venueWebsite = 'https://www.vectron.com.au'

//sign up page setup
export const isMultiVenue = true
export const accountMatch = true

export const isGaming = false //need to be true to allow gamingOdyssey or gamingIGT
export const gamingOdyssey = false
export const gamingIGT = false

export const showTierSignup = false //need to be true to allow isCustomField and showMultipleExtended
export const isCustomField = false //need to be true to allow showMultipleExtended
export const showMultipleExtended = false

//home page setup
export const isShowStarBar = true
export const showTierBelowName = false
export const showPointsBelowName = false
export const isShowPreferredVenue = true
export const showTier = true

export const isGroupFilter = false // must be multi venue and has venue tags
export default vars
