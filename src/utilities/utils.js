import {Appearance, Dimensions, Linking, Platform, StatusBar} from 'react-native'
import Geolocation from 'react-native-geolocation-service'
import {isIOS} from '../Themes/Metrics'

export function getStatusBarHeight(skipAndroid = false) {
  if (Platform.OS === 'ios') {
    return isIphoneX() ? 65 : 30
  }
  if (skipAndroid) {
    return 0
  }

  return StatusBar.currentHeight
}
export function isIphoneX() {
  const dimen = Dimensions.get('window')
  return (
    Platform.OS === 'ios' &&
    !Platform.isPad &&
    !Platform.isTVOS &&
    (dimen.height === 812 || dimen.width === 812 || dimen.height === 896 || dimen.width === 896)
  )
}

export function validateEmail(email) {
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  return re.test(email)
}

export function isEmptyValues(value) {
  return (
    value === undefined ||
    value === null ||
    value === NaN ||
    (typeof value === 'object' && Object.keys(value).length === 0) ||
    (typeof value === 'string' && value.trim().length === 0)
  )
}

export function isDarkMode() {
  return Appearance.getColorScheme() === 'dark'
}

export function urlToBlob(url) {
  return new Promise((resolve, reject) => {
    let xhr = new XMLHttpRequest()
    xhr.onerror = reject
    xhr.onreadystatechange = () => {
      if (xhr.readyState === 4) {
        resolve(xhr.response)
      }
    }
    xhr.open('GET', url)
    xhr.responseType = 'blob' // convert type
    xhr.send()
  })
}

export const updateObject = (oldObject, updatedValues) => {
  return {
    ...oldObject,
    ...updatedValues,
  }
}

export function openMaps(data) {
  Linking.openURL(`https://maps.google.com/?saddr=${data.start}&daddr=${data.end}`)
}

export function formatNumber(number) {
  if (!number) return ''
  if (typeof number === 'string') return number
  return number.toString().match(new RegExp('.{1,4}$|.{1,4}', 'g')).join('-')
}
export function getIconsName(name) {
  switch (name) {
    case 'website':
      return 'globe'
    case 'food':
      return 'utensils'
    case 'drink':
      return 'coffee'
    default:
      return name
  }
}

